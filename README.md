# О проекте onear.me
Главная идея заключается в том, чтобы сделать общение в интернете безопаснее.
Мы считаем, что чат не должен хранить сообщения, которые пользователь не желает
сохранять. На текущий момент большенство чатов только блокирует возможность
видеть сообщение, после того как пользователь его "удаляет". У другого
пользователя данное сообщение продолжает существовать.

Данный чат не хранит информацию о Вас, Вашем местоположении и всех переданных
Вами сообщениях. Всё отправляется напрямую людям, которые находятся в одной
комнате с Вами.


# Преимущества
* Не хранит персональную информацию о Вас
* Не хранит информацию о Вашем местоположении
* Не хранит информацию о всех переданных Вами сообщениях

---
# MIT License

Copyright (c) 2016 ideas cup


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:


The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.


THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.