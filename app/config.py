import os
from datetime import timedelta


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SEND_FILE_MAX_AGE_DEFAULT = 60
    CACHE_TYPE = 'simple'
    MONGODB_SETTINGS = dict(DB='onearme_test')
    MAILER_SENDER = 'no-reply@ideascup.me'


class ProductionConfig(Config):
    SERVER_NAME = 'onear.me'
    SECRET_KEY = 'onearme_ideascup_production'
    JSONIFY_PRETTYPRINT_REGULAR = False
    MONGODB_SETTINGS = dict(DB='onearme')


class StageConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    SERVER_NAME = 'stage.onear.me'
    SECRET_KEY = 'onearme_ideascup_stage'
    DEBUG_TB_INTERCEPT_REDIRECTS = False  #: Дебаг при редиректе
    DEBUG_TB_PANELS = (
        # 'flask_debugtoolbar.panels.versions.VersionDebugPanel',
        'flask_debugtoolbar.panels.timer.TimerDebugPanel',
        'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask_debugtoolbar.panels.config_vars.ConfigVarsDebugPanel',
        'flask_debugtoolbar.panels.template.TemplateDebugPanel',
        'flask_debugtoolbar.panels.logger.LoggingPanel',
        'flask_debugtoolbar.panels.route_list.RouteListDebugPanel',
        # 'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
        # 'flask_debugtoolbar.panels.sqlalchemy.SQLAlchemyDebugPanel',
        'flask_mongoengine.panels.MongoDebugPanel')


class MigrateConfig(Config):
    pass
