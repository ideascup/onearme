import os
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, request

app = Flask(__name__, static_url_path='')
app.config.from_object(os.environ['APP_SETTINGS'])

app.jinja_env.add_extension('jinja2.ext.do')

handler = RotatingFileHandler('app.log', maxBytes=10000, backupCount=1)
handler.setLevel(logging.INFO)
app.logger.addHandler(handler)


from app.controllers import base_controller, room_controller, user_controller

app.register_blueprint(base_controller)
app.register_blueprint(room_controller, url_prefix='/room')
app.register_blueprint(user_controller, url_prefix='/user')