from _datetime import datetime
from mongoengine import StringField, DateTimeField, IntField, ListField, \
    BooleanField
from core.database import nosql

    
class Room(nosql.Document):
    create_at = DateTimeField(default=datetime.utcnow)
    update_at = DateTimeField(default=datetime.utcnow)
    name = StringField()
    avatar_id = StringField()
    lifetime = IntField(min_value=0, default=3600)
    message_lifetime = IntField(min_value=0, default=60)
    user_ids = ListField(StringField())
    is_dialog = BooleanField()
    is_hidden = BooleanField()