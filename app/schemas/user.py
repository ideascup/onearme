from _datetime import datetime
from mongoengine import StringField, DateTimeField
from core.database import nosql


class User(nosql.Document):
    create_at = DateTimeField(default=datetime.utcnow)
    update_at = DateTimeField()
    login = StringField(required=True, unique=True)
    password = StringField(required=True)
    avatar_id = StringField()
    name = StringField()
    token_hash = StringField()

    def public_info(self):
        return {'id': self.id, 'name': self.name,
                'avatarInfo': {'url': '/user/%s/avatar/' % self.id}}

