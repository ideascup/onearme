from _datetime import datetime
from mongoengine import StringField, DateTimeField
from core.database import nosql


class Contact(nosql.Document):
    create_at = DateTimeField(default=datetime.utcnow)
    update_at = DateTimeField()
    owner_id = StringField(require=True)
    user_id = StringField(require=True)
    name = StringField()