from _datetime import datetime
from mongoengine import StringField, DateTimeField
from core.database import nosql


class Attachment(nosql.Document):
    create_at = DateTimeField(default=datetime.utcnow)
    user_id = StringField()
    room_id = StringField()
    filename = StringField(required=True)