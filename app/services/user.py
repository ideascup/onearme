from hashlib import sha256
from uuid import uuid1
from wtforms import Form


def hashing_password(login:str, password:str):
    string = '%s.%s.%s' % (login.lower(), 'onearme', password)
    return sha256(string.encode()).hexdigest()
    
def hashing_password_by_form(form:Form):
    return hashing_password(form.login.data, form.password.data)
    
def generate_token(uid, login:str):
    string = '%s.%s.%s' % (str(uid), uuid1().hex, login.lower())
    return sha256(string.encode()).hexdigest()