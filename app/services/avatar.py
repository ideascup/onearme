from math import floor
from re import compile
from io import BytesIO
from PIL import Image, ImageDraw, ImageFont, ImageFilter, ImageEnhance
from flask import send_file


FONT = '/home/dima/c9/projects/onear.me/src/Roboto-Light.ttf'


colors = ('c1ffc1', 'c1ffc1', 'ffffbe', 'b2b2b2', 'b2c1c1', 'ffffbc', 'b2babd',
          'baffbd', 'ffffbb', 'bbffb2', 'ffffbd', 'ffbabd', 'ffffc1', 'ffffb2',
          'ffdebe', 'ffb2b2', 'ffffbc', 'f2bfb5', 'ffc1c1', 'ffffb2', 'bab2b2',
          'ffbab4', 'c0ffbe', 'ffffb8', 'c1ffc1', 'c0ffb2', 'efefb5', 'ffffb3',
          'ffffb2', 'c1ffb2', 'ffc1c1', 'b2b2b2', 'b2bab2', 'bab2b2', 'ffffb2',
          'ffbebd', 'b2bab2', 'b2bab2', 'ffb2b2', 'bebeb2', 'ffffb2')


def get_color_by_text(text):
    """ Возвращает цвет в hex по кол-ву символов в тексте """
    colorIndex = len(text) % len(colors)
    return '#' + colors[colorIndex]
    
    
def get_abbreviation_by_text(text):
    """ Генерирует и возвращает аббревиатуру первых двух слов текста """
    prog = compile('(?:^|\s)([^$-/:-?{-~!"^_`\[\]])')
    matches = [m.group(1).upper() for m in prog.finditer(text)]
    return ''.join(matches[:2])


def draw_avatar(object_, size=40):
    """ Рисует дефолтную аватарку для чата """
    color = get_color_by_text(object_.name)
    layer = Image.new('RGB', (size, size), color)
    
    font_layer = Image.new('RGBA', (size, size))
    
    abbreviation = get_abbreviation_by_text(object_.name)
    font_size = floor(size / 2.2)
    font = ImageFont.truetype(FONT, font_size)
    w, h = font.getsize(abbreviation)
    text_position = (int((size - w)/1.9), int((size - h)/2.4))
    
    ImageDraw.Draw(font_layer) \
        .text(text_position, abbreviation, font=font, fill='#000000bb')

    img = Image.composite(font_layer, layer, font_layer)
    
    img_io = BytesIO()
    img.save(img_io, 'JPEG')
    
    img_io.seek(0)
    return img_io
    

def send_avatar(*args, **kwargs):
    """ Отправляет нарисованую аватарку пользователю """
    return send_file(draw_avatar(*args, **kwargs), mimetype='image/jpeg')
