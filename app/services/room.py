from hashlib import sha256


def hashing_rid(rid):
    string = 'rid.%s' % str(rid)
    return sha256(string.encode()).hexdigest()