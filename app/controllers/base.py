from flask import Blueprint, request, jsonify, session, Response
from app.schemas import User, Room
from core.services import Page
from core.cupcore.filemanager import FileUploader
from ..resources.forms import RegistrationUserForm, AuthorizationUserForm, \
    RoomSettingsForm, RoomSecureLinkForm, AddContactForm
from ..services.room import hashing_rid
from uuid import uuid4


base_controller = Blueprint('base', __name__)


images = FileUploader('_storage/images/',
                      extensions={'jpg', 'jpeg', 'png', 'gif', 'svg', 'bmp'})


@base_controller.route('/')
def main():
    user = request.cookies.get('uid')
    room = session.get('room')
    
    return Page(request.endpoint).render(
        user=user, room=room,
        authorization_form=AuthorizationUserForm(request.form),
        registration_form=RegistrationUserForm(request.form),
        room_settings_form=RoomSettingsForm(request.form),
        secure_link_form=RoomSecureLinkForm(request.form),
        add_contact_form=AddContactForm(request.form))


@base_controller.route('/upload/', methods=['POST'])
def upload_file():
    user_id = request.cookies.get('uid')
    room_id = request.args.get('rid')
    
    room = Room.objects(id=room_id, user_ids=user_id).first()
    if room is None:
        return jsonify(error=1)
    
    hash_rid = hashing_rid(room_id)
    
    files = request.files.getlist('image') \
        + request.form.getlist('image')
    filenames = [images.save(file, prefix=hash_rid) for file in files]
    if not len(filenames):
        return jsonify(error=1)
    return jsonify(success=1, filenames=filenames)
    

@base_controller.route('/uploaded/<filename>')
def uploaded_file(filename):
    user_id = request.cookies.get('uid')
    room_id = request.args.get('rid')
    
    room = Room.objects(id=room_id, user_ids=user_id).first()
    if room is None:
        return Page.not_found()
    
    return images.send(filename)


# TODO: Отправлять сообщения через tor
#
# @base_controller.route('/say/<lang>/<message>')
# def speach_text(lang, message):
#     headers = {
#         'Host': 'translate.google.com',
#         'User-Agent': 'Mozilla/5.0 (1; Linux x86_64) '
#                       'AppleWebKit/537.36 (KHTML, like Gecko) '
#                       'Chrome/53.0.2785.101 Safari/537.36'
#     }
#   
#     # lang = 'ru'
#     # message = 'привет'
#     token = GoogleToken().calculate_token(message)
#    
#     payload = dict(ie='UTF-8', total=1, idx=0, textlen=len(message), q=message,
#                   tl=lang, tk=token, client='t', prev='input')
#     r = requests.get('http://translate.google.com/translate_tts', params=payload, headers=headers)
#     return Response(r.content, mimetype='audio/ogg')
