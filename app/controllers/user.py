from uuid import uuid1
from hashlib import md5
from flask import Blueprint, request, jsonify, session, current_app
from core.services import Page
from core.cupcore.filemanager import FileUploader
from app.schemas import User, Contact, Room, Attachment
from ..resources.forms import RegistrationUserForm, AuthorizationUserForm, \
    AddContactForm
from ..services.user import hashing_password_by_form, generate_token
from ..services.avatar import send_avatar
from mongoengine.errors import NotUniqueError


user_controller = Blueprint('user', __name__)


user_avatars = FileUploader('_storage/user_avatars/',
                            extensions={'jpg', 'jpeg', 'png', 'bmp'})


@user_controller.route('/', methods=['POST'])
def signup():
    if not request.is_xhr:
        return Page.not_found()
    if request.cookies.get('uid') is not None:
        return Page.not_found()
    
    form = RegistrationUserForm(request.form)
    if not form.validate():
        return jsonify(errors=form.errors)
    
    user = User(
        login=form.login.data.lower(),
        password=hashing_password_by_form(form),
        name=form.name.data or form.login.data)
    user.save()
    
    return Page.redirect('user.signin', 307)
        
        
@user_controller.route('/signin', methods=['POST'])
def signin():
    if not request.is_xhr:
        return Page.not_found()
    if request.cookies.get('uid') is not None:
        return Page.not_found()
        
    form = AuthorizationUserForm(request.form)
    if not form.validate():
        return jsonify(errors=form.errors)
        
    user = User.objects(
        login=form.login.data.lower(),
        password=hashing_password_by_form(form)). \
        first()
    if user is None:
        return jsonify(errors=[])
    
    user.token_hash = generate_token(user.id, user.login)
    user.save()
    
    server_name = current_app.config.get('SERVER_NAME')
    response = jsonify(success=1)
    response.set_cookie('uid', str(user.id), domain='.%s' % server_name)
    response.set_cookie('ut', user.token_hash, domain='.%s' % server_name)
    
    return response


@user_controller.route('/logout')
def logout():
    if session.get('room'):
        del session['room']
    # deprecated
    if session.get('user'):
        del session['user']
    
    server_name = current_app.config.get('SERVER_NAME')
    response = Page.redirect('base.main')
    response.set_cookie('uid', '', domain='.%s' % server_name, expires=0)
    response.set_cookie('ut', '', domain='.%s' % server_name, expires=0)
    
    return response


@user_controller.route('/<user_id>/avatar/')
def avatar(user_id):
    current_user_id = request.cookies.get('uid')
    if not current_user_id:
        return Page.not_found()
    
    user = User.objects(id=user_id).first()
    if not user:
        return Page.not_found()
    
    contact = Contact.objects(owner_id=current_user_id, user_id=user_id).first()
    if contact:
        user.name = contact.name
        
    if not user.avatar_id:
        avatar_size = int(request.args.get('s', '40'))
        return send_avatar(user, size=avatar_size)
    
    avatar = Attachment.objects(id=user.avatar_id).first()
    if not avatar:
        user.avatar_id = None
        user.save()
        
    return user_avatars.send(avatar.filename)
    

@user_controller.route('/avatar/', methods=['POST'])
def upload_avatar():
    user_id = request.cookies.get('uid')
    if not user_id:
        return jsonify(error=1)
        
    user = User.objects(id=user_id).first()
    if not user:
        return jsonify(error=1)

    files = request.files.getlist('image') \
        + request.form.getlist('image')
    filename = user_avatars.save(files[0], prefix=user_id)
    if not filename:
        return jsonify(error=1)
    
    attachment = Attachment(owner_id=user_id, filename=filename)
    attachment.save()
    
    user.avatar_id = str(attachment.id)
    user.save()
        
    return jsonify(success=1)
    
    
@user_controller.route('/contacts/', methods=['POST'])
def add_contact():
    if not request.is_xhr or not request.cookies.get('uid'):
        return Page.not_found()
        
    user_id = request.cookies.get('uid')
    user = User.objects(id=user_id).first()
    if user is None:
        return jsonify(errors=[])
        
    form = AddContactForm(request.form)
    if not form.validate():
        return jsonify(errors=form.errors)
    
    contact_user = User.objects(login=form.login.data.lower()).first()
    contact_uid = str(contact_user.id)
    if Contact.objects(owner_id=user_id, user_id=contact_uid).first() \
            is not None:
        return jsonify(errors=dict(login='Такой контакт уже существует'))
    
    contact = Contact(
        owner_id=user_id,
        user_id=contact_uid,
        name=form.name.data)
    contact.save()

    if contact.name:
        contact_user.name = contact.name
    
    return jsonify(success=1, user=contact_user.public_info())
    
    
@user_controller.route('/<user_id>/dialog/', methods=['POST'])
def create_dialog(user_id):
    current_user_id = request.cookies.get('uid')
    if not current_user_id:
        return jsonify(error=1)
        
    dialog = Room.objects(
        user_ids__all=(current_user_id, user_id),
        is_dialog=True).\
        first()
        
    if dialog is not None:
        return jsonify(success=1, rid=str(dialog.id))
        
    dialog = Room(
        user_ids=(current_user_id, user_id),
        is_dialog=True,
        is_hidden=True,
        lifetime=0,
        message_lifetime=0)
    dialog.save()
    
    return jsonify(success=1, rid=str(dialog.id))
