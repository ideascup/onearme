from _datetime import datetime
from uuid import uuid4
from flask import Blueprint, request, jsonify, session, current_app
from core.services import Page
from core.cupcore.filemanager import FileUploader
from app.schemas import Room, User, Attachment
from ..resources.forms import RoomSettingsForm, RoomSecureLinkForm, \
    AuthorizationUserForm
from ..services.user import hashing_password_by_form, generate_token
from ..services.avatar import send_avatar


room_controller = Blueprint('room', __name__)


room_avatars = FileUploader('_storage/room_avatars/',
                            extensions={'jpg', 'jpeg', 'png', 'bmp'})
                            

@room_controller.route('/', methods=['POST'])
def create():
    form = RoomSettingsForm(request.form)
    if not form.validate():
        return jsonify(errors=form.errors)
        
    user_id = request.cookies.get('uid')
    if not user_id:
        return jsonify(error=1)
    
    room = Room(name=form.name.data,
                user_ids=[user_id],
                lifetime=form.lifetime.data,
                message_lifetime=form.message_lifetime.data)
    room.save()
    
    return jsonify(success=1, rid=str(room.id))


@room_controller.route('/', methods=['PUT'])
def update():
    form = request.form;
    form_data = {key: form.get(key) for key in form.keys() if key != 'rid'}
    
    room = Room.objects(id=form.get('rid')).first()
    if not room:
        return jsonify(error=1)
    
    for key, value in form_data.items():
        setattr(room, key, value)
    
    room.update_at = datetime.utcnow();
    room.save()
    
    return jsonify(success=1)


@room_controller.route('/<room_id>/avatar/')
def icon(room_id):
    current_user_id = request.cookies.get('uid')
    if not current_user_id:
        return Page.not_found()
        
    room = Room.objects(id=room_id, user_ids=current_user_id).first()
    if not room:
        return Page.not_found()
    
    if not room.avatar_id:
        if not room.name and room.is_dialog:
            return Page.not_found()
        
        return send_avatar(room)
    
    avatar = Attachment.objects(id=room.avatar_id).first()
    if not avatar:
        room.avatar_id = None
        room.save()
        
    return room_avatars.send(avatar.filename)
    

@room_controller.route('/<room_id>/avatar/', methods=['POST'])
def upload_icon(room_id):
    current_user_id = request.cookies.get('uid')
    if not current_user_id:
        return jsonify(error=1)
        
    room = Room.objects(id=room_id, user_ids=current_user_id).first()
    if not room:
        return jsonify(error=1)

    files = request.files.getlist('image') \
        + request.form.getlist('image')
    filename = room_avatars.save(files[0], prefix=user_id)
    if not filename:
        return jsonify(error=1)
    
    attachment = Attachment(room_id=room_id, filename=filename)
    attachment.save()
    
    room.avatar_id = str(attachment.id)
    room.save()
        
    return jsonify(success=1)