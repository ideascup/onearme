from wtforms import Form, StringField, IntegerField, HiddenField, PasswordField, \
    BooleanField
from wtforms.validators import DataRequired, NumberRange, Length, EqualTo, \
    ValidationError
from core.cupcore.wtforms_ext import RangeField
from app.schemas import User, Contact


class RegistrationUserForm(Form):
    login = StringField('Логин', [
        DataRequired('Логин не может быть пустым')])
    password = PasswordField('Придумайте пароль', [
        Length(min=6, message='Пароль должен иметь минимум 6 символов')])
    confirm = PasswordField('Подтвердите пароль', [
        EqualTo('password', message='Пароли не совпадают')])
    name = StringField('Отображаемый ник')
    
    def validate_login(self, field):
        if User.objects(login=field.data.lower()).first() is not None:
            raise ValidationError('Данный логин уже занят')
    
    def validate_password(self, field):
        password_length = len(field.data)
        if password_length > 0 and password_length < 6:
            raise ValidationError('Пароль должен состоять минимум из 6 символов')
        

class AuthorizationUserForm(Form):
    login = StringField('Логин', [
        DataRequired('Введите свой логин')])
    password = PasswordField('Введите пароль', [
        DataRequired('Введите свой пароль')])
        
        
class AddContactForm(Form):
    login = StringField('Логин контакта', [
        DataRequired('Поле не должно быть пустым')])
    name = StringField('Имя контакта')
    
    def validate_login(self, field):
        if User.objects(login=field.data.lower()).first() is None:
            raise ValidationError('Пользователь не найден')
        
        
class RoomSettingsForm(Form):
    name = StringField('Название')
    description = StringField('Описание')
    icon = HiddenField()
    lifetime = RangeField(
        'Минуты жизни комнаты *', default=60, min=0, max=360, step=10)
    message_lifetime = RangeField(
        'Секунды жизни сообщений *', default=60, min=0, max=360, step=10)
    user_ids = HiddenField()
                                         

class RoomSecureLinkForm(Form):
    rid = HiddenField()
    nickname = StringField("Ваш ник")
