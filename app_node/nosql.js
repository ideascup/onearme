const {mongo} = require('./config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo.host}/${mongo.dbname}`);

const db = mongoose.connection;
db.on('error', (m) => console.log('connection error:', m));
db.once('open', () => console.log('mongodb connected'));

/**
 * Тип модели MongoDB для параметра _id
 * 
 * @typedef {string} ObjectId
 */


module.exports = {
    'User': require('./models/user'),
    'Room': require('./models/room'),
    'Message': require('./models/message'),
    'Contact': require('./models/contact')
};
