const each = require('lodash/each');
const {User, Room, Message} = require('../nosql');
const Channels = require('../common/channels');



module.exports = socket => {
    const wss = socket._server;
    
    return async data => {
        if (!socket.hasRoom(data.rid))
            return;
            
        if (!data.text && data.images && !data.images.length)
            return;

        try {
            const room = await Room.findById(data.rid);
            if (room.is_hidden) {
                await Room.update(
                    {'_id': data.rid},
                    {$unset: {'is_hidden': 1}});
                
                each(room.user_ids, uid => {
                    each(wss.socketsByUserId[uid + ''], socket_ => {
                        socket_._run(
                            Channels.JOIN,
                            {'rid': data.rid},
                            {'from_server': true});
                    });
                });
            }
            
            const createAt = Date.now();
            const removeAt = room.getMessageRemoveTime();
            
            let messageData = {
                'room_id': data.rid,
                'owner_id': socket.user.id,
                'user_ids': room.user_ids,
                'text': data.text,
                'images': data.images,
                'create_at': createAt
            };
            
            if (removeAt !== null) {
                messageData['remove_at'] = removeAt;
            }
            
            let message = new Message(messageData);
            await message.save();
                
            socket.all(Channels.MESSAGE, await message.getInfo());
            
            User.findByIdAndUpdateDate(socket.user.id, (err, user) => {
                if (err)
                    return console.error(err);
            });
            
            Room.findByIdAndUpdateDate(data.rid, (err, room) => {
                if (err)
                    return console.error(err);
                
                socket.all(Channels.CHANGE_INFO, {
                    'rid': room._id,
                    'removeTime': room.getRemoveTime()
                });
            });
        } catch (e) {
            return console.error(e);
        }
    };
};