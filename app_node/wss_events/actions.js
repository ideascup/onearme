const each = require('lodash/each');
const {Message} = require('../nosql');
const Actions = require('../common/actions');
const Channels = require('../common/channels');



module.exports = socket => {
    return async data => {
        const currentUser = socket.user;
        let messages;
        
        switch (data.action) {
            case Actions.DELETE:
                messages = await Message.find({
                    '_id': {$in: data.mids},
                    'room_id': data.rid
                });
                
                let ownerMessagesIds = [];
                
                each(messages, message => {
                    if (message.owner_id == currentUser.id) {
                        ownerMessagesIds.push(message._id);
                        message.remove();
                    } else {
                        message.removeUser(currentUser.id);
                    }
                });
                
                socket.broadcast(Channels.ACTIONS, {
                    'action': Actions.DELETE,
                    'rid': data.rid,
                    'mids': ownerMessagesIds
                });
                break;
                
            case Actions.SAVE:
                messages = await Message.find({
                    '_id': {$in: data.mids},
                    'room_id': data.rid,
                    'remove_at': {$exists: true}
                });
                
                each(messages, async msg => {
                    msg.removeUser(currentUser.id);
                    
                    let message = new Message({
                        'room_id': msg.room_id,
                        'owner_id': msg.owner_id,
                        'user_ids': [socket.user.id],
                        'text': msg.text,
                        'images': msg.images,
                        'create_at': msg.create_at
                    });
                    await message.save();
                    
                    let messageData = await message.getInfo();
                    socket.send(Channels.MESSAGE, messageData);
                });
                break;
                
            case Actions.EDIT:
                let message = await Message.findOne({
                    '_id': data.mid,
                    'room_id': data.rid,
                    'owner_id': currentUser.id
                });
                
                if (!message)
                    return;
                
                message.text = data.text;
                message.is_edited = true;
                await message.save();
                
                socket.all(Channels.ACTIONS, {
                    'action': Actions.EDIT,
                    'rid': data.rid,
                    'mid': data.mid,
                    'message': await message.getInfo()
                });
                break;
        }
    };
};