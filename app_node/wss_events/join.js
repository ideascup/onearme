const each = require('lodash/each');
const filter = require('lodash/filter');
const {User, Room, Message, Contact} = require('../nosql');
const {ErrorCodes} = require('../common/channel_error');
const Channels = require('../common/channels');


module.exports = socket => {
    const wss = socket._server;
    
    return async ({rid: roomId}, {from_server}) => {
        if (!roomId)
            return;
        
        const currentUser = socket.user;
        
        try {
            let room = await Room.findOne({
                '_id': roomId,
                'user_ids': currentUser.id
            });
            if (!room || room.isExpired())
                return socket.sendError(ErrorCodes.ROOM_NOT_EXISTS);
            
            socket.addRoom(roomId);
            if (!room.is_hidden) {
                let messages = await Message
                    .find({'user_ids': currentUser.id, 'room_id': roomId})
                    .sort({'create_at': -1})
                    .limit(30);
                    
                messages = await Promise.all(
                    messages.map(async msg => await msg.getInfo()));
                
                socket.send(Channels.JOINED, {
                    'rid': roomId,
                    'messages': messages
                });
                
                if (!from_server) {
                    const createAt = Date.now();
                    socket.broadcast(Channels.JOINED, {
                        'type': 'joined',
                        'rid': roomId,
                        'user': currentUser,
                        'createAt': createAt,
                        'removeTime': createAt + 15000
                    });
                }
            }
            
            let users = await User.find({'_id': {$in: room.user_ids}});
            let usersData = users.map(user => user.getPublicInfo());
            let roomInfo = room.getInfo();
            
            socket.all(Channels.CHANGE_INFO, {
                'rid': roomId,
                'info': roomInfo,
                'users': usersData,
                'onlineUids': wss.getOnlineUserIds(room.user_ids),
                'removeTime': room.getRemoveTime()
            }); 
            
            // if (roomInfo.isDialog) {
            //     const contactUids = filter(
            //         room.user_ids,
            //         uid => uid + '' != currentUser.id);
            //     const contact = await Contact.findOne({
            //         'owner_id': currentUser.id,
            //         'user_id': contactUids[0]
            //     });
                
            //     each(users, user => {
            //         if (user._id + '' !== currentUser.id) {
            //             if (contact
            //                     && contact.user_id === user._id + ''
            //                     && contact.name) {
            //                 user.name = contact.name;
            //             }
                        
            //             socket.send(Channels.CHANGE_INFO, {
            //                 'rid': roomId,
            //                 'info': user.getPublicInfo()
            //             });
                        
            //             return false;
            //         }
            //     });
            // }
        } catch (e) {
            return console.error(e);
        }
    };
};