const Channels = require('../common/channels');


module.exports = {
    [Channels.CONNECT]: require('./connect'),
    [Channels.JOIN]: require('./join'),
    [Channels.TYPING]: require('./typing'),
    [Channels.MESSAGE]: require('./message'),
    [Channels.ACTIONS]: require('./actions')
};