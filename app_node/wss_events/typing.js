const {nl2br, escape_} = require('../supports');
const Channels = require('../common/channels');


module.exports = socket => {
    return async (data) => {
        if (!socket.hasRoom(data.rid))
            return;

        const createAt = Date.now();
        let removeTimeOffset = 3000;
        let messageType = 'typing';

        if (data.text != null) {
            if (data.text === '')
                return;
            
            data.text = nl2br(escape_(data.text));
            messageType = 'live';
            removeTimeOffset = 6000;
        }
        
        socket.broadcast(Channels.TYPING, {
            'rid': data.rid,
            'user': socket.user,
            'text': data.text,
            'type': messageType,
            'createAt': createAt,
            'removeTime': createAt + removeTimeOffset
        });
    };
};