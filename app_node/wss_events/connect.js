const {User, Room} = require('../nosql');
const {ErrorCodes} = require('../common/channel_error');
const Channels = require('../common/channels');


module.exports = socket => {
    let wss = socket._server;
    
    return async data => {
        if (socket.cookies.uid == null
                || socket.cookies.ut == null) {
            socket.close();
            return;
        }
        
        try {
            const currentUser = await User.findCurrentUser(socket);
            if (!currentUser)
                return socket.sendError(ErrorCodes.INVALID_TOKEN);
                
            socket.user = currentUser.getPublicInfo();
            
            const uid = socket.user.id;
            wss.socketsByUserId[uid] || (wss.socketsByUserId[uid] = []);
            wss.socketsByUserId[uid].push(socket);
            
            const rooms = await Room.find({
                'user_ids': uid,
                'is_hidden': {$exists: false}
            });
            const roomIds = rooms
                .filter(room => !room.isExpired())
                .map(room => room._id);
                
            const contacts = await currentUser.getUserList();
            
            socket.send(Channels.CONNECTED, {
                'currentUser': socket.user,
                'roomIds': roomIds,
                'contacts': contacts
            });
        } catch (e) {
            return console.error(e);
        }
    };
};