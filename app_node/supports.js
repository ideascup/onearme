const crypto = require('crypto');
const path = require('path');
const eachRight = require('lodash/eachRight');
const del = require('del');


function md5(string_) {
    return crypto.createHash('md5').update(string_).digest('hex');
}

function escape_(string_) {
    return string_.replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

function nl2br(string_) {
    return string_.replace(/\n\r|\n|\r/g, '<br>');
}

function getTime(date) {
    return date ? new Date(date).getTime() : undefined;
}

function deleteImages(images) {
    eachRight(images, filename => {
        del(path.resolve(__dirname, '../app/_storage/images/', filename));
    });
}


module.exports = {
    'md5': md5,
    'escape_': escape_,
    'nl2br': nl2br,
    'time': getTime,
    'deleteImages': deleteImages
};