const DEVELOPMENT = 'dev';
const NODE_ENV = process.env.NODE_ENV || DEVELOPMENT; 
const isDev = NODE_ENV === DEVELOPMENT;


let config = {
    TEST: isDev,
    PORT: isDev ? 3010 : 3011,
    mongo: {
        host: 'localhost',
        dbname: `onearme${isDev ? '_test' : ''}`
    }
};


module.exports = config;