const server = require('http').createServer();
const WebSocketServer = require('./websocket').Server;
const config = require('./config');
const Channels = require('./common/channels');
const Actions = require('./common/actions');
const Daemon = require('./common/daemon');
const Models = require('./nosql');
const each = require('lodash/each');
const {deleteImages} = require('./supports');
const wssEvents = require('./wss_events/_all');

server.listen(config.PORT, () => console.log(`server listen on ${config.PORT} port`));

const wss = new WebSocketServer(server, {
    path: '/'
});

wss.on('connection', function (socket) {
    config.TEST && console.log(`connect socket ${socket.id}`);
    
    each(wssEvents, (initHandler, channelName) => {
        socket.on(channelName, initHandler(socket));
    });
});


(new Daemon({delay: 10000}))
    .onTick(async function () {
        try {
            let rooms = await Models.Room.find({'lifetime': {$gt: 0}});
            
            rooms.filter(room => room.isExpired()).forEach(room => {
                let socketIds = wss.rooms[room.id];
                
                if (socketIds && socketIds.length) {
                    each(socketIds, sid => {
                        let roomIds = wss.sockets[sid].roomIds;
                        roomIds.splice(roomIds.indexOf(room.id), 1);
                    });
                    
                    const socket = wss.sockets[socketIds[0]];
                    socket.all(Channels.DESTRUCTION, {
                        'rid': room.id
                    });
                }
                
                room.remove();
                delete wss.rooms[room.id];
            });
        } catch (e) {
            return console.error(e);
        }
        
        try {
            let messagesByRoom = {};
            let messages = await Models.Message.find(
                {'remove_at': {$lt: Date.now()}});
            
            each(messages, message => {
                if (!messagesByRoom[message.room_id]) {
                    messagesByRoom[message.room_id] = [];
                }
                messagesByRoom[message.room_id].push(message._id);
                
                deleteImages(message.images);
                message.remove();
            });
            
            each(messagesByRoom, (mids, rid) => {
                let socketIds = wss.rooms[rid];
                if (!socketIds || !socketIds.length)
                    return;
                
                let socket = wss.sockets[socketIds[0]];
                socket.all(Channels.ACTIONS, {
                    'action': Actions.DELETE,
                    'rid': rid,
                    'mids': mids
                });
            });
        } catch (e) {
            return console.error(e);
        }
        
        // Models.User.find({
        //     create_at: {$lt: Date.now() - Timelifes.ANON_USER}
        // }, (err, users) => {
        //     if (err) {
        //         return console.error(err);
        //     }
            
        //     // todo: Необходимо дисконнектить пользователей при удалении,
        //     //       если они находятся в чате
        //     each(users, user => user.remove());
        // });
    })
    .start();