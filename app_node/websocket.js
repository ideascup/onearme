const each = require('lodash/each');
const eio = require('engine.io');
const colors = require('colors');
const cookie = require('cookie');
const Channels = require('./common/channels');
const {ErrorMessages} = require('./common/channel_error');

/**
 * События
 * 
 * @namespace WebSocketEvents
 * @property {string} REQUEST       Получение данных сервером
 * @property {string} CONNECT       Соединение с сокетом
 * @property {string} CLOSE         Завершение соединения с сокетом
 * @property {string} MESSAGE       Получение сообщения
 */
const Events = {
    CONNECT: 'connection',
    CLOSE: 'close',
    MESSAGE: 'message'
};

/**
 * События, поддерживаемые сервером
 * @type {WebSocketEvents[]}
 * @ignore
 */
const SERVER_SUPPORT_EVENTS = [Events.CONNECT, Events.CLOSE];


/**
 * @class Реализация сокета для работы с чатом
 */
class WSSocket {
    /**
     * Создает объект для сокет-соединения пользователя
     * 
     * @param {Server} server       WebSocket сервер
     * @param {Socket} eioSocket    engine.io socket
     */
    constructor(server, eioSocket) {
        /** @type {Server} */
        this._server = server;
        /** @type {Socket} */
        this._socket = eioSocket;
        /** @type {object.<Function[]>} */
        this._events = {};
        /** @type {string} */
        this.id = this._socket.id;
        /** @type {ObjectId[]} */
        this.roomIds = [];
        /** @type {Object.<string>} */
        this.cookies = {};
        /** @type {object} */
        this.user = {};
        
        this._initialize();
    }
    
    /**
     * Иниациализация сокета
     * 
     * @protected
     */
    _initialize() {
        this._server.sockets[this.id] = this;
        this._parseCookies();
        
        console.log(
            'Socket joined'.bold,
            colors.blue(this.id),
            'On server'.bold,
            colors.red(Object.keys(this._server.sockets).length)
        );
        
        this._bindEvents();
    }
    
    /**
     * Добавление обработчиков на события сокета
     * 
     * @protected
     */
    _bindEvents() {
        this._socket.on(Events.MESSAGE, (data) => {
            if (typeof data === 'string') {
                try {
                    data = JSON.parse(data);
                } catch (e) {
                    data = {};
                }
            }
            
            if (!data.channel)
                return;
            
            this._run(data.channel, data);
        });
        
        this._socket.on(Events.CLOSE, () => {
            this._server.removeSocket(this);
            
            console.log(
                'Socket left'.bold,
                colors.blue(this.id),
                'On server'.bold,
                colors.red(Object.keys(this._server.sockets).length)
            );
        });
    }
    
    /**
     * Запускает обработчики события сокета по названию канала
     * 
     * @param {string} channelName      Название канала
     * @param {object} data             Объект, который передается в обработчики
     * @param {object} [privateData]    Приватный объект, передаваемый с сервера
     * 
     * @protected
     */
    _run(channelName, data, privateData={}) {
        const handlers = this._events[channelName];
        if (data.channel) {
            delete data.channel;
        }
        
        if (Array.isArray(handlers)) {
            this._parseCookies();
            handlers.every(handler => !handler.call(this, data, privateData));
        }
    }
    
    /**
     * Парсит строку cookie запроса и сохраняет в публичную перменную
     * 
     * @protected
     */
    _parseCookies() {
        this.cookies = cookie.parse(this._socket.request.headers.cookie || '');
    }
    
    /**
     * Добавление обработчика на канал
     * 
     * @param {string} channelName      Название канала
     * @param {Function} handler        Обработчик события для канала
     * 
     * @return {Socket}
     */
    on(channelName, handler) {
        if (typeof handler !== 'function')
            return;
        
        if (Array.isArray(channelName)) {
            each(channelName, name => this.on(name, handler));
            return;
        }
        
        this._events[channelName] || (this._events[channelName] = []);
        this._events[channelName].push(handler);
        return this;
    }
    
    /**
     * Отменяет все обработчики у канала
     * 
     * @param {string} channelName      Название канала
     * 
     * @return {Socket}
     */
    off(channelName) {
        delete this._events[channelName];
        return this;
    }
    
    /**
     * Отправляет сообщение сокету
     * 
     * @param {string} channelName      Название канала
     * @param {object} [data={}]        Передаваемый json объект
     */
    send(channelName, data={}) {
        data.channel = channelName;
        this._socket.send(JSON.stringify(data));
    }

    /**
     * Отправляет клиенту сообщение об ошибке
     * 
     * @param {number} errorCode        Код ошибки
     */
    sendError(errorCode) {
        this.send(Channels.ERROR, {
            'code': errorCode,
            'message': ErrorMessages[errorCode]
        });
    }
    
    /**
     * Отправляет сообщение всем в комнату, кроме данного пользователя
     * 
     * @param {string} channelName      Название канала
     * @param {object} data             Передаваемый json объект
     */
    broadcast(channelName, data) {
        if (!data || !data.rid)
            return;
        
        const _roomSocketIds = this._server.rooms[data.rid];
        const _sockets = this._server.sockets;
        
        each(_roomSocketIds, id => {
            if (id !== this.id) {
                _sockets[id].send(channelName, data);
            }
        });
    }
    
    /**
     * Отправляет сообщение всем в комнату
     * 
     * @param {string} channelName      Название канала
     * @param {object} data             Передаваемый json объект
     */
    all(channelName, data) {
        if (!data || !data.rid)
            return;
        
        const _roomSocketIds = this._server.rooms[data.rid];
        const _sockets = this._server.sockets;
        
        each(_roomSocketIds, id => _sockets[id].send(channelName, data));
    }
    
    /**
     * Добавляет сокет в комнату
     * 
     * @param {ObjectId} roomId         id модели Room
     */
    addRoom(roomId) {
        const hasRoom = this.roomIds.some(rid => roomId === rid);
        if (!hasRoom) {
            this.roomIds.push(roomId);
        }
        
        this._server.changeRoomInfo(roomId, this.id);
    }

    /**
     * Находится ли данный пользователь в комнате
     * 
     * @param {ObjectId} roomId         id модели 'Room'
     * 
     * @return {boolean} Находится ли в комнате
     */
    hasRoom(roomId) {
        if (!roomId || this.roomIds.indexOf(roomId) === -1)
           return false;
        return true;
    }
    
    /**
     * Закрывает соединение с сокетом
     */
    close() {
        this._socket.close();
    }
}


/**
 * @class Надстройка над вебскокет сервером
 */
class WSServer {
    /**
     * Создает объект сервера
     * 
     * @param {HttpServer} httpServer   http server
     * @param {object=} config          Конфигурация для engine.io-server
     */
    constructor(httpServer, config={}) {
        this.wss = eio.attach(httpServer, config);
        this.sockets = {};
        this.socketsByUserId = {};
        this.rooms = {};
        this._events = {};
        this._bindEvents();
    }
    
    /**
     * Добавление обработчиков на события сервера
     * @protected
     */
    _bindEvents() {
        let _eventName = 'connection';
        this.wss.on(_eventName, (eioSocket) => {
            const socket = new WSSocket(this, eioSocket);
            this._events[_eventName] && this._events[_eventName](socket);
        });
    }
    
    /**
     * Добавление обработчика на событие сервера
     * 
     * @param {string} eventName        Название события
     * @param {Function} handler        Обработчик события сервера
     * @return {Server}
     */
    on(eventName, handler) {
        if (SERVER_SUPPORT_EVENTS.indexOf(eventName) === -1
                || typeof handler !== 'function') {
            return this;
        }
        
        this._events[eventName] = handler;
        return this;
    }
    
    /**
     * Удаляет сокет списков сокетов и комнат
     * 
     * @param {Socket} socket       Сокет соединение пользователя
     */
    removeSocket(socket) {
        delete this.sockets[socket.id];
        
        each(socket.roomIds, rid => {
            this.rooms[rid] = this.rooms[rid].filter(sid => sid !== socket.id);
        });
        
        let userSockets = this.socketsByUserId[socket.user.id + ''];
        if (userSockets) {
            let socketIndex = userSockets.indexOf(socket);
            userSockets.splice(socketIndex, 1);
        }
    }
    
    /**
     * Добавляет ID сокета в комнату или обновляет информацию
     * 
     * @param {ObjectId} roomId     id модели 'Room'
     * @param {string} socketId     ID соединения сокета
     */
    changeRoomInfo(roomId, socketId) {
        const _room = this.rooms[roomId] || (this.rooms[roomId] = []);
        
        const hasSocket = _room.some((sid, i) => {
            // todo: какая-то ересь. Нужно протестировать
            if (sid === socketId) {
                _room[i] = socketId;
                return true;
            }
            
            return false;
        });
        
        if (!hasSocket) {
            _room.push(socketId);
        }
    }
    
    /**
     * Возвращает список пользователей онлайн
     * 
     * @param {Array.<ObjectId>} userIds    Список id пользователей
     * 
     * @return {Array.<string>} Список id пользователей
     */
    getOnlineUserIds(userIds) {
        let onlineUserIds = [];
        each(this.sockets, ({user:{id}}) => {
            if (id && userIds.indexOf(id) > -1
                    && onlineUserIds.indexOf(id + '') === -1) {
                onlineUserIds.push(id + '');
            }
        });
        
        return onlineUserIds;
    }
}


module.exports = {
    Server: WSServer,
    Socket: WSSocket
};
