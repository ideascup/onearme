const mongoose = require('mongoose');
const {Schema} = mongoose;
const {md5, time} = require('../supports');


let RoomSchema = Schema({
    create_at: {type: Date, default: Date.now},
    update_at: {type: Date, default: Date.now},
    name: String,
    avatar_id: String,
    lifetime: {type: Number, min: 0, default: 60},
    message_lifetime: {type: Number, min: 0, default: 60},
    user_ids: [String],
    is_dialog: Boolean,
    is_hidden: Boolean
}, {collection: 'room'});


let {statics, methods} = RoomSchema;

    
statics.findByIdAndUpdateDate = function (id, callback) {
    return this.findOneAndUpdate(
        {'_id': id},
        {$set: {'update_at': Date.now()}},
        callback
    );
};


methods.getInfo = function () {
    let info = {
        'lifetime': this.lifetime,
        'messageLifetime': this.message_lifetime,
        'isDialog': this.is_dialog
    };
    
    if (this.name) {
        info['name'] = this.name || this._id;
        info['avatarInfo'] = {'url': `/room/${this._id}/avatar/`};
    }
    
    return info;
};

methods.getRemoveTime = function () {
    return this.lifetime ?
        time(this.update_at) + this.lifetime * 60000
        : null;
};

methods.getMessageRemoveTime = function () {
    return this.message_lifetime ?
        Date.now() + this.message_lifetime * 1000
        : null;
};

methods.isExpired = function () {
    let removeTime = this.getRemoveTime();
    return removeTime !== null && removeTime <= Date.now();
};


module.exports = mongoose.model('Room', RoomSchema);