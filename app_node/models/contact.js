const mongoose = require('mongoose');
const {Schema} = mongoose;


let ContactSchema = Schema({
    owner_id: {type: String, required: true},
    user_id: {type: String, required: true},
    name: String
}, {collection: 'contact'});


module.exports = mongoose.model('Contact', ContactSchema);