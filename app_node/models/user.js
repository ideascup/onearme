const mongoose = require('mongoose');
const {Schema} = mongoose;
const each = require('lodash/each');
const filter = require('lodash/filter');
const Contact = require('./contact');


let UserSchema = Schema({
    create_at: {type: Date, default: Date.now},
    update_at: {type: Date, default: Date.now},
    login: {type: String, index: {unique: true}},
    password: String,
    avatar_id: String,
    name: String,
    token_hash: String
}, {collection: 'user'});


let {methods, statics} = UserSchema;


statics.findByIdAndUpdateDate = function (id, callback) {
    return this.findOneAndUpdate(
        {'_id': id},
        {$set: {'update_at': Date.now()}},
        callback
    );
};

statics.findCurrentUser = function (socket) {
    return this.findOne({
        '_id': socket.cookies.uid,
        'token_hash': socket.cookies.ut
    });
};


methods.getPublicInfo = function () {
    return {
        'id': this._id + '',
        'name': this.name,
        'avatarInfo': {'url': `/user/${this._id}/avatar/`}
    };
};

methods.getUserList = async function () {
    let contacts = await Contact.find({'owner_id': this._id});
    if (!contacts.length)
        return [];
        
    let user_ids = contacts.map(contact => contact.user_id);
    let users = await mongoose.model('User').find({'_id': {$in: user_ids}});
    
    each(contacts, ({user_id, name:contactName}, i) => {
        each(users, (user, j) => {
            if (user_id === user._id + '') {
                if (contactName && contactName.trim()) {
                    user.name = contactName.trim();
                }
                
                contacts[i] = users.splice(j, 1)[0];
                return false; // break;
            }
        });
    });
    
    return filter(contacts, 'name')
        .map(user => user.getPublicInfo());
};


module.exports = mongoose.model('User', UserSchema);
