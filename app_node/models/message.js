const mongoose = require('mongoose');
const {Schema} = mongoose;
const {time, escape_, nl2br, deleteImages} = require('../supports');
const User = require('./user');


let MessageSchema = Schema({
    create_at: {type: Date, default: Date.now},
    room_id: {type: String, required: true},
    owner_id: {type: String, required: true},
    user_ids: [String],
    text: String,
    images: [String],
    remove_at: Date,
    is_edited: {type: Boolean, default: false}
}, {collection: 'message'});


let {methods} = MessageSchema;


methods.getInfo = async function () {
    let owner = await User.findById(this.owner_id);
    let ownerInfo = owner.getPublicInfo();
    
    console.log(ownerInfo);
    
    return {
        'id': this._id,
        'rid': this.room_id,
        'text': nl2br(escape_(this.text)),
        'images': this.images,
        'user': ownerInfo,
        'createAt': time(this.create_at),
        'removeTime': time(this.remove_at),
        'isEdited': this.is_edited
    };
};

methods.removeUser = function (userId) {
    this.user_ids.splice(this.user_ids.indexOf(userId), 1);
    
    if (this.user_ids.length) {
        return this.save();
    } else {
        deleteImages(this.images);
        return this.remove();
    }
};


module.exports = mongoose.model('Message', MessageSchema);