/**
 * Коды ошибок
 * 
 * @instance
 * @alias ErrorCodes
 * @property {number} INVALID_TOKEN=1001    Передан не корректный токен
 * @property {number} ROOM_NOT_EXISTS=1002  Комнаты не существует
 * 
 * @memberof ChannelErrors
 */
let Codes = {
    INVALID_TOKEN: 1001,
    ROOM_NOT_EXISTS: 1002
};

/**
 * Объект с сообщениями об ошибках по коду ошибок
 * 
 * @instance
 * @alias ErrorMessages
 * @memberof ChannelErrors
 * 
 * @see {@link ChannelErrors#ErrorCodes}
 */
let Messages = {
    [Codes.INVALID_TOKEN]: 'Передан не корректный токен',
    [Codes.ROOM_NOT_EXISTS]: 'Комнаты не существует'
};

/**
 * @namespace ChannelErrors
 */
module.exports = {
    ErrorCodes: Codes,
    ErrorMessages: Messages
};
