const assign = require('lodash/assign');

/**
 * Настройки демона по умолчанию
 * 
 * @property {number} delay     Задержка перед выполнением событий
 * 
 * @memberof {Daemon}
 * @private
 */
const DEFAULT_SETTINGS = {
    delay: 1000
};

/**
 * Состояния демона
 * 
 * @property {number} RUNNED    Запущен
 * @property {number} PAUSED    На паузе
 * @property {number} STOPPED   Остановлен
 * 
 * @memberof {Daemon}
 * @private
 */
const States = {
    RUNNED: 0,
    PAUSED: 1,
    STOPPED: 2
};


/**
 * @class Демон, выполняющий процессы в фоновом режиме
 */
class Daemon {
    /**
     * Создает демона, который выполняет события в фоновом режиме
     * 
     * @param {object} config           конфигурация демона
     * @param {number} config.delay     задержка перед выполнением событий
     */
    constructor(config) {
        this.config = assign({}, DEFAULT_SETTINGS, config);
        this._handlers = [];
        this._state = States.STOPPED;
        this._process = null;
    }
    
    /**
     * Getter
     * Возвращает задержку выполнения тика
     * @return {number}
     */
    get processDelay() {
        return this._process && this._process.delay ?
            this._process.delay
            : this.config.delay;
    }
    
    /**
     * Добавляет обработчик события на каждый тик
     * 
     * @param {Function} handler    Обработчик события 'tick'
     * @return {Daemon}
     */
    onTick(handler) {
        const hasHandler = this._handlers.some(fn => handler === fn);
        if (!hasHandler)
            this._handlers.push(handler);
        return this;
    }
    
    /**
     * Создает процесс 'tick' с задержкой 'processDelay'
     */
    tick() {
        if (this._process && this._process.id)
            return;

        this._process = {
            'id': setTimeout(() => {
                for (let i = 0; i < this._handlers.length; i++) {
                    if (this._handlers[i]() === true) {
                        break;
                    }
                }
                
                this.stop();
                this.tick();
            }, this.processDelay),
            'startTime': Date.now()
        };
    }
    
    /**
     * Запускает демона
     * 
     * @return {Daemon}
     */
    start() {
        if (this._state === States.RUNNED) {
            this.stop();
        }
        
        this._state = States.RUNNED;
        this.tick();
        return this;
    }
    
    /**
     * Останавливает демона
     * 
     * ```
     * daemon.stop()
     * ```
     */
    stop() {
        if (this._process === null)
            return;
        
        this._state = States.STOPPED;
        clearTimeout(this._process.id);
        this._process = null;
    }
    
    /**
     * Ставит процесс демона на паузу
     */
    pause() {
        if (this._process === null)
            return;
        
        this._state = States.PAUSED;
        clearTimeout(this._process.id);
        this._process.id = null;
        this._process.delay = Date.now() - this._process.startTime;
    }
}


module.exports = Daemon;
