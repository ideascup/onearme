
/**
 * Каналы чата
 * 
 * @namespace Channels
 * @property {string} ERROR         Сообщения об ошибках
 * @property {string} CONNECT       Запрос соединения с сервером
 * @property {string} CONNECTED     Успешно соединен с сервером
 * @property {string} JOIN          Запрос войти в комнату
 * @property {string} JOINED        Успешно вошел в комнату
 * @property {string} LEAVE         Вышел из комнаты
 * @property {string} MESSAGE       Передача сообщениями
 * @property {string} TYPING        Передача 'живыми' сообщениями
 * @property {string} DESTRUCTION   Команда удаления
 * @property {string} CHANGE_INFO   Изменение информации комнаты
 * @property {string} ACTIONS       Выполнение заданых действий с сервера
 */
module.exports = {
    ERROR: 'error',
    CONNECT: 'connect',
    CONNECTED: 'connected',
    JOIN: 'join',
    JOINED: 'joined',
    LEAVE: 'leave',
    TYPING: 'typing',
    MESSAGE: 'message',
    DESTRUCTION: 'destruction',
    CHANGE_INFO: 'change_info',
    ACTIONS: 'actions'
};
