
/**
 * Действия для канала Channels.ACTIONS
 * 
 * @namespace Actions
 * @property {string} DELETE    Удаление сообщений
 * @property {string} SAVE      Сохранение сообщений
 * @property {string} EDIT      Редактирование сообщения
 */
module.exports = {
    DELETE: 'delete',
    SAVE: 'save',
    EDIT: 'edit'
};