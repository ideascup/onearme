import Channels from 'channels';


export default function (wsc) {
    return data => {
        // в списке менять состояние что человек печатает (даже при live)
        // в чате выводить надпись если печатает (при live - сообщение)
        this.messager.createMessage(data, Channels.TYPING);
    };
}