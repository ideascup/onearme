import each from 'lodash/each';
import User from 'models/user';
import Channels from 'channels';


export default function (wsc) {
    return ({currentUser, roomIds, contacts}) => {
        this.currentUser = new User(currentUser);
        
        this.roomList.clear();
        each(roomIds, rid => {
            wsc.send(Channels.JOIN, {'rid': rid});
        });
        
        this.userList.clear();
        each(contacts, contact => {
            this.userList.addItem(new User(contact));
        });
    };
}