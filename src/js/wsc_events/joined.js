import User from 'models/user';


export default function (wsc) {
    return data => {
        if (data.user) {
            data.user = new User(data.user);
            if (data.user.uid !== this.currentUser.uid) {
                this.messager.createMessage(data);
            }
            return;
        }
        
        let room = this.messager.addRoom(data);
        this.roomList.addItem(room);
    };
}