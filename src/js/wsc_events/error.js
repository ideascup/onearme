/*global localStorage, location*/
import {ErrorCodes} from 'channel_error';


export default function (wsc) {
    return ({code, message}) => {
        switch (code) {
            case ErrorCodes.INVALID_TOKEN:
                localStorage.clear();
                location.pathname = '/user/logout';
                break;
            default:
                console.error(message);
        }
    };
}