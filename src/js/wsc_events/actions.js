import eachRight from 'lodash/eachRight';
import Actions from 'actions';
import User from 'models/user';


export default function (wsc) {
    return data => {
        let room = this.messager.rooms.get(data.rid);
        if (!room)
            return;
        
        switch (data.action) {
            case Actions.DELETE:
                eachRight(room.messages, msg => {
                    if (data.mids.indexOf(msg.data.id) > -1) {
                        msg.remove();
                    }
                });
                break;
                
            case Actions.EDIT:
                for (let i = 0; i < room.messages.length; i++) {
                    let message = room.messages[i];
                    if (message.data.id === data.mid) {
                        data.message.user = new User(data.message.user);
                        message.editAndRender(data.message);
                        break;
                    }
                }
                break;
        }
    };
}