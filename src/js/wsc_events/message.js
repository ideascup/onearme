import ICNotification from 'ic/notification/base';
import User from 'models/user';


export default function (wsc) {
    return data => {
        if (!this._isActiveBrowserTab
                || !this.messager.isOpened
                || this.messager.rooms.activeId !== data.rid) {
            new ICNotification({
                title: data.user.name,
                body: data.text.replace(/<br>/g, '\n'),
                icon: data.user.avatarInfo.url,
                sound: '/sound/notification_a.mp3',
                onclick: notification => {
                    let $item = this.roomList.$list.find(`[data-rid="${data.rid}"]`);
                    this.messager.changeRoom($item[0]);
                    window.focus();
                    notification.close();
                }
            });
        }
        
        this.messager.createMessage(data);
    };
}