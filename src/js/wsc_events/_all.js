import Channels from 'channels';
// handlers
import error from './error';
import joined from './joined';
import typing from './typing';
import message from './message';
import actions from './actions';
import connected from './connected';
import destruction from './destruction';
import changeInfo from './change_info';


module.exports = {
    [Channels.ERROR]: error,
    [Channels.JOINED]: joined,
    [Channels.TYPING]: typing,
    [Channels.MESSAGE]: message,
    [Channels.ACTIONS]: actions,
    [Channels.CONNECTED]: connected,
    [Channels.CHANGE_INFO]: changeInfo,
    [Channels.DESTRUCTION]: destruction
};