export default function (wsc) {
    return data => {
        this.messager.rooms.get(data.rid).changeInfo(data);
    };
}