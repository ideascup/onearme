import jQuery from 'jQuery';
import reduce from 'lodash/reduce';
import concat from 'lodash/concat';
import app from 'components/app';
import Room from 'models/room';


class RoomsManager {
    constructor () {
        this._rooms = {};
        this._activeRoomId = null;
    }
    
    /**
     * __Getter__
     * Возвращает активную комнату
     * 
     * @type {Room}
     */
    get active() {
        return this.get(this._activeRoomId);
    }
    
    /**
     * __Getter__
     * Возвращает id активной комнаты
     * 
     * @type {ObjectId}
     */
    get activeId() {
        return this._activeRoomId;
    }
    
    /**
     * Возвращает комнату по id
     * 
     * @param {ObjectId} roomId     id модели Room
     * @param {boolean} force       Принудительно создает комнату, если её нет.
     */
    get(roomId, force) {
        if (force) {
            this._rooms[roomId] || (this._rooms[roomId] = new Room(roomId));
        }
        return this._rooms[roomId];
    }
    
    /**
     * Возвращает список всех комнат
     * 
     * @return {Array.<Room>}   Список комнат
     */
    getList() {
        return this._rooms;
    }
    
    /**
     * Меняет комнату
     * 
     * @param {ObjectId} roomId     Идентификатор модели комнаты
     */
    changeRoom(roomId) {
        let room = this.get(roomId, true);
        if (this._activeRoomId === room.rid)
            return;
        this._activeRoomId = room.rid;
        
        room.renderInfo();
        room.renderAdditionalInfo();
    }
    
    /**
     * Возвращает список всех сообщений со всех существующих комнат
     * 
     * @return {Message[]}
     */
    getAllMessages() {
        return reduce(this._rooms, (result, {messages}) => {
            return concat(result, messages);
        }, []);
    }
}


export {RoomsManager as default};