import each from 'lodash/each';
import app from 'components/app';
import Activity from 'components/activities/base';
import Preference from 'ic/preferences/base';
import DialogPreference from 'ic/preferences/dialog';


class SettingsActivity extends Activity {
    constructor(node, name, url) {
        super(node, name, url);
        this.$preferenceList = null;
        this.preferences = {};
        this._initialize();
    }
    
    _initialize() {
        this.$preferenceList = this.$root.find('.list--preferences');
        each(this.$preferenceList.children(), node => {
            let key = node.getAttribute('ic-key');
            this.preferences[key] = this.getPreferenceByNode(node);
            this.preferences[key].onBeforeUpdate = function (data) {
                data['rid'] = app.messager.activeRoom.rid;
            };
            this.preferences[key].onUpdateSuccess = function (data) {
                key = this.key.replace(/_(\w)/, (m, m1) => m1.toUpperCase());
                app.messager.activeRoom[key] = this._value;
            };
        });
    }
    
    getPreferenceByNode(node) {
        switch (node.getAttribute('ic-type')) {
            case 'text':
            case 'range':
                return new DialogPreference(node);
            default: return new Preference(node);
        }
    }
    
    open() {
        let activeRoom = app.messager.activeRoom;
        if (!activeRoom)
            return;
        
        each(activeRoom.preferences, (value, key) => {
            this.preferences[key].value = value;
        });
        
        super.open();
    }
    
    onOpen() {
        
    }
}


Activity.Classes['settings'] = SettingsActivity;