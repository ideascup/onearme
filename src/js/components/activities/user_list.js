import jQuery from 'jQuery';
import Channels from 'channels';
import WebSocketClient from 'components/websocket_client';
import Activity from './base';
import ListActivity from './list';
import app from 'components/app';

import itemTemplate from './templates/user_list.pug';


/**
 * @class Активности со списком пользователей
 * @extends ListActivity
 */
class UserListActivity extends ListActivity {
    /**
     * Создает представление активности со списком пользователей
     */
    constructor(node, name, url) {
        super(node, name, url);
        this.userIds = [];
        this.itemTemplate = itemTemplate;
    }
    
    /**
     * @inheritdoc
     */
    onItemClick($item) {
        let user_id = $item.data('uid');
        
        jQuery.ajax({
            'method': 'post',
            'url': `/user/${user_id}/dialog/`
        }).done(({rid:room_id}) => {
            let room = app.messager.rooms.get(room_id, true);
            if (!room.name) {
                let wsc = WebSocketClient();
                wsc.send(Channels.JOIN, {'rid': room_id});
            }
            
            app.messager.changeRoom(room_id);
        });
    }
    
    /**
     * Поиск элемента списка по id пользователя
     * 
     * @param {ObjectId} roomId     id модели пользователя
     */
    findItemById(userId) {
        return this.findItem(`[data-uid="${userId}"]`);
    }
    
    /**
     * Добавление элемента пользователя в список
     * 
     * @param {User} user       Модель пользователя
     * 
     * @override
     */
    addItem(user) {
        const userId = user.uid;
        if (!userId || this.userIds.indexOf(userId) > -1)
            return;
        
        this.userIds.push(userId);
        super.addItem(user.locals);
    }
    
    /**
     * Удаление элемента пользователя из списка
     * 
     * @param {ObjectId} userId     id модели пользователя
     * 
     * @override
     */
    removeItem(userId) {
        const index = this.userIds.indexOf(userId);
        if (index === -1)
            return;
            
        this.userIds.splice(index, 1);
        this.findItemById(userId).remove();
    }
    
    /**
     * @inheritdoc
     */
    clear() {
        super.clear();
        this.userIds = [];
    }
}


Activity.Classes['list:users'] = UserListActivity;


export {UserListActivity as default};