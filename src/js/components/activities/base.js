/* global location */
import jQuery from 'jQuery';
import each from 'lodash/each';
import isFunction from 'lodash/isFunction';

/**
 * Список всех созданых activity
 * 
 * @enum {Activity}
 * @ignore
 */
let __activities = {};

/**
 * История открытий активностей
 * 
 * @enum {string}
 * @ignore
 */
let __history = [];

/**
 * Открытая активность
 * 
 * @type {Activity}
 * @ignore
 */
let __openedActivity = null;

/**
 * Состояния в котором может быть активити
 * 
 * @property {string} OPENED        активити открыто
 * @property {string} CLOSED        активити скрыто
 * 
 * @memberof Activity
 */
const States = {
    OPENED: 0,
    CLOSED: 1
};

/**
 * Классы состояний, используемые в активностях
 * 
 * @ignore
 */
const Classes = {
    OPENED: 'is-opened',
    LOCKED: 'is-locked'
};

/**
 * Настройки активности, разбитые по состояниям
 * 
 * @ignore
 */
const configByState = [
    {
        animateFrom: {
            opacity: 0,
            left: 60
        },
        animateTo: {
            opacity: 1,
            left: 0
        }
    },
    {
        animateFrom: {
            left: 0,
            zIndex: 9000
        },
        animateTo: {
            left: -60
        },
    }
];

/**
 * @class Активность, отвечающая за все основные функции
 */
class Activity {
    /**
     * Создает объекта представляения activity
     * 
     * @param {string|HTMLElement} node    Главный элемент активности
     * @param {string=} name               Название activity
     * @param {string=} url                URL-адрес данной активности
     */
    constructor(node, name, url) {
        this.$root = jQuery(node);
        this.name = name || this.$root.attr('ic-activity');
        this.url = url || this.$root.attr('ic-url');
        
        if (typeof node === 'string')
            node = this.$root[0];
            
        if (!node)
            return;
            
        if (__activities[this.name])
            return __activities[this.name];
        __activities[this.name] = this;
        
        this.status = this.isOpened ? States.OPENED : States.CLOSED;
        if (this.isOpened)
            this._changeState(this.status, true);
            
        this.__lock = false;
    }
    
    get isOpened() {
        return this.isLocked || this.$root.hasClass(Classes.OPENED);
    }
    
    get isLocked() {
        return this.$root.hasClass(Classes.LOCKED);
    }
    
    set isLocked(isLocked) {
        let method = isLocked ? 'addClass' : 'removeClass';
        this.$root[method](Classes.LOCKED);
    }
    
    /**
     * Меняет состояние активности на указанное
     * 
     * @param {string} activityState    Состояние активности
     * @param {bool=} fast              Флаг мгновенной смены состояния
     * @param {Function=} callback      Вызывается после смены состояния
     * 
     * @private
     */
    _changeState(activityState, fast, callback) {
        if (this.__lock) return;
        this.__lock = true;

        const config = configByState[activityState];
        const lastOpenedActivity = __openedActivity;
        
        
        if (activityState === States.OPENED) {
            if (this === __openedActivity && !fast) {
                fast = true;
            } else {
                __openedActivity = this;
                __history.push(this.name);
            }
            
            this.$root.addClass(Classes.OPENED);
            location.hash = this.url;
            lastOpenedActivity && lastOpenedActivity.close();
        } else if (this === __openedActivity) {
            /** 
             * Закрываем текущее активити
             * 
             * Стираем из истории 2 последних активности, т.к. предпоследняя
             * добавится позже при открытии
             */
            __openedActivity = null;
            const activityName = __history.splice(-2, 2)[0];
            Activity.getByName(activityName).open();
        }

        this.$root
            .css(config.animateFrom)
            .animate(
                config.animateTo,
                fast ? 0 : 300,
                () => {
                    if (activityState === States.CLOSED)
                        this.$root.removeClass(Classes.OPENED);
                    this.$root[0].removeAttribute('style');
                    this.status = activityState;

                    if (isFunction(callback)) {
                        callback(this);
                    }

                    this.__lock = false;
                }
            );
    }
    
    /**
     * Открывает активити
     */
    open() {
        if (this.$root.hasClass(Classes.LOCKED))
            return;
        
        this._changeState(States.OPENED, false, () => this.onOpen());
    }
    
    /**
     * Закрывает активити
     */
    close() {
        if (this.$root.hasClass(Classes.LOCKED))
            return;
        
        this._changeState(States.CLOSED, false, () => this.onClose());
    }
    
    /**
     * Событие, вызываемое при открытии активности
     */
    onOpen() {}
    
    /**
     * Событие, вызываемое при закрытии активности
     */
    onClose() {}
    
    /**
     * Получение активности по имени
     * 
     * @param {string} name     Название активности
     * 
     * @return {Activity}
     */
    static getByName(name) {
        return __activities[name];
    }
    
    /**
     * Получение активности по url
     * 
     * @param {string} url      Ссылка активности
     * 
     * @return {Activity}
     */
    static getByUrl(url) {
        let result = null;
        
        for (let name in __activities) {
            if (__activities[name].url === url) {
                result = __activities[name];
            }
        }
        
        return result;
    }
    
    /**
     * Возвращает открытую активность
     * 
     * @return {Activity}
     */
    static getOpened() {
        return __openedActivity;
    }
    
    /**
     * Инициализация активностей в HTML
     */
    static initialize() {
        const $activities = jQuery('.activity[ic-activity]');
        each($activities, node => {
            let activityType = node.getAttribute('ic-type');
            let _Activity = activityType ? this.Classes[activityType] : Activity;
            if (!_Activity)
                return;
            
            new _Activity(node);
        });
        
        jQuery(document).on('click', '[ic-open-activity]', function (e) {
            const activityName = this.getAttribute('ic-open-activity');
            Activity.getByName(activityName).open();
            e.preventDefault();
            return false;
        }).on('click', '[ic-close-activity]', function (e) {
            if (__history.length > 1) {
                window.history.back();
            }
            e.preventDefault();
            return false;
        });
        
        window.addEventListener('hashchange', () => {
            // todo: добавить возможность изменять активности по изменению hash
            if (location.hash && location.hash[1] !== '/')
                return;
            
            let url = location.hash.slice(1);
            if (__openedActivity.url === url)
                return;
            
            let activity = Activity.getByUrl(url);
            __history = __history.slice(0, __history.indexOf(activity.name));
            
            activity.open();
        });
    }
}

Activity.States = States;
Activity.Classes = {};

export {Activity as default};