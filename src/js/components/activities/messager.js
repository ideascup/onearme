import jQuery from 'jQuery';
import each from 'lodash/each';
import eachRight from 'lodash/eachRight';
import Channels from 'channels';
import Actions from 'actions';
import WebSocketClient from 'components/websocket_client';
import Activity from 'components/activities/base';
import RoomManager from 'components/rooms_manager';
import ICImageUploader from 'ic/uploader/image';
import ICBasePopup from 'ic/popup/base';
import {emoji, lists} from 'components/emoji';
import {sprintf} from 'sprintf-js';
import User from 'models/user';

import previewTemplate from 'image_preview.pug';

let wsc = null;

/**
 * @class Активность чата
 * @extends Activity
 */
class MessagerActivity extends Activity {
    /**
     * Создает представление активности чата
     */
    constructor(node, name, url) {
        super(node, name, url);
        
        // Заголовок комнат
        this.$actionsHeader = this.$root.find('.activity__header--actions');
        this.$closeActionsButton = this.$actionsHeader.find('.activity__back');
        this.$actionsHeaderTitle = this.$actionsHeader.find('>h2');
        this.$actionDeleteButton = this.$actionsHeader.find('.js-action-delete');
        this.$actionSaveButton = this.$actionsHeader.find('.js-action-save');
        this.$actionEditButton = this.$actionsHeader.find('.js-action-edit');
        
        this.$roomHeader = this.$root.find('.activity__header:not(.activity__header--actions)');
        this.$roomIcon = this.$roomHeader.find('.activity__avatar');
        this.$roomTitle = this.$roomHeader.find('>h2');
        this.$unreadButton = this.$roomHeader.find('.messager__unread-button');
        
        // Список сообщений
        this.$roomBody = this.$root.find('.activity__body');
        this.$messageList = this.$roomBody.find('.list');
        
        // Создание сообщения
        this.$textarea = this.$root.find('textarea');
        this.$textareaWrapper = this.$textarea.parent();
        this.$textareaVirtual = jQuery('<pre>');
        this.$sendButton = this.$root.find('.messager__send');
        this.$editButton = this.$root.find('.messager__edit');
        this.$attachButton = this.$root.find('.messager__attach');
        
        // Смайлики
        this.$emojiContainer = this.$root.find('.emoji-container');
        this.$emojiTabs = this.$emojiContainer.find('.emoji-container__tab');
        this.$emojiList = this.$emojiContainer.find('.emoji-container__list');
        this.$emojiButton = this.$root.find('.messager__emoji');
        
        this.rooms = new RoomManager;
        this.selectedMessages = [];
        this.isEditMessage = false;
        
        this._initialize();
    }
    
    /**
     * __Getter__
     * Возвращает активную комнату
     */
    get activeRoom() {
        return this.rooms.active;
    }
    
    /**
     * Инициализация активити
     * @protected
     */
    _initialize() {
        wsc = WebSocketClient();
        
        jQuery('<div style="height:0;overflow:hidden;">')
            .append(this.$textareaVirtual)
            .appendTo(this.$textareaWrapper);
        
        let fileInput = this.$attachButton.children()[0];
        this.imageUploader = new ICImageUploader(fileInput, {
            preview: this.$root.find('.messager__preview'),
            previewTemplate: previewTemplate,
            onRenderPreview($item) {
                let img = $item.find('img')[0];
                if (img.width < img.height) {
                    $item.addClass('is-vertical');
                }
            },
            onUploaded: ({filenames}) => {
                if (filenames && filenames.length) {
                    this.activeRoom.images = filenames;
                    this.$roomBody.addClass('has-preview');
                }
            }
        });
        
        function copyTextToClipboard(text) {
            let error = false;
            let textarea = document.createElement('textarea');
            textarea.style.position = 'fixed';
            textarea.style.top = 0;
            textarea.style.left = 0;
            textarea.style.width = '2em';
            textarea.style.height = '2em';
            textarea.value = text;
            document.body.appendChild(textarea);
            textarea.select();
            try {
                error = !document.execCommand('copy');
            } catch (err) { 
                error = true;
            }
            document.body.removeChild(textarea);
            return error;
        }
        
        this._bindEvents();
    }
    
    /**
     * Добавление обработчиков на события активности
     * @protected
     */
    _bindEvents() {
        this.$sendButton.on('click', () => {
            if (this.isEditMessage
                    || !this.$textarea.val() && !this.activeRoom.images.length)
                return;
            
            let error = wsc.send(Channels.MESSAGE, {
                'rid': this.rooms.activeId,
                'text': this.$textarea.val(),
                'images': this.activeRoom.images
            });
            
            if (error) {
                // todo: Необходимо добавлять в стек, чтобы отправить позже
                return;
            }
            
            this.onAfterSend();
        });
        
        this.$editButton.on('click', () => {
            if (!this.isEditMessage)
                return;
            
            let editedMessage = this.selectedMessages[0];
            wsc.send(Channels.ACTIONS, {
                'action': Actions.EDIT,
                'rid': this.rooms.activeId,
                'mid': editedMessage.data.id,
                'text': this.$textarea.val()
            });
            
            this.onAfterSend();
            this.$closeActionsButton.trigger('click');
        });

        this.$textarea.on({
            'focus blur input change': () => {
                this.$textareaVirtual.text(this.$textarea.val() + '_');
                
                const _height = this.$textareaVirtual.height();
                this.$textarea.height(_height)
                    .parent().height(_height);
            },
            'input': () => {
                let data = {
                    'rid': this.rooms.activeId,
                    'startTime': Date.now()
                };
                
                wsc.send(Channels.TYPING, data);
            },
            'keydown': ({shiftKey, key}) => {
                if (!shiftKey && key === 'Enter') {
                    let $button = this.isEditMessage ?
                        this.$editButton
                        : this.$sendButton;
                    $button.trigger('click');
                    return false;
                }
            },
            'paste': ({originalEvent:{clipboardData:{items}}}) => {
                if (!items)
                    return;
                
                let files = [];
                
                each(items, item => {
                    if (item.kind === 'file') {
                        files.push(item.getAsFile());
                    }
                });
                
                if (files.length) {
                    this.imageUploader.uploadFiles(files);
                }
            }
        });
        
        this.$messageList.on('scroll', () => {
            let room = this.activeRoom;
            let unreadMessages = room.unreadMessages;
            if (!unreadMessages.length)
                return;
            
            const _scrollTop = this.$messageList.scrollTop();
            const _listHeight = this.$messageList.height();
            
            each(unreadMessages, message => {
                if (_scrollTop >= message.offsetTop - _listHeight) {
                    message.isNotReaded = false;
                }
            });
        });
        
        this.$closeActionsButton.on('click', () => {
            if (this.isEditMessage) {
                this.isEditMessage = false;
                this.$actionsHeader.removeClass('is-edit-message');
                this.$textarea.val('').change();
            }
            
            this.unselectMessages();
        });
        
        this.$unreadButton.on('click', (evt) => {
            let unreadMessages = this.activeRoom.unreadMessages;
            if (!unreadMessages.length)
                return;
            
            this.$messageList.animate({
                scrollTop: unreadMessages[0].offsetTop - this.$messageList.height() / 2
            }, 800);
            
            evt.preventDefault();
            return false;
        });
        
        this.$actionDeleteButton.on('click', () => {
            if (!this.selectedMessages.length)
                return;
                
            let mids = this.selectedMessages.map(msg => msg.data.id);
            
            wsc.send(Channels.ACTIONS, {
                'action': Actions.DELETE,
                'rid': this.rooms.activeId,
                'mids': mids
            });
            
            eachRight(this.selectedMessages, msg => msg.remove());
        });
        
        this.$actionSaveButton.on('click', () => {
            let messages = this.getSelectedTempMessages();
            let mids = messages.map(msg => msg.data.id);
                
            wsc.send(Channels.ACTIONS, {
                'action': Actions.SAVE,
                'rid': this.rooms.activeId,
                'mids': mids
            });
            
            eachRight(messages, msg => msg.remove());
        });
        
        this.$actionEditButton.on('click', () => {
            if (this.selectedMessages.length !== 1)
                return;
            
            let editedMessage = this.selectedMessages[0];
            this.isEditMessage = true;
            this.$actionsHeader.addClass('is-edit-message');
            this.$actionsHeaderTitle.text('Редактирование');
            
            let text = editedMessage.data.text
                .replace(/<br>/ig, '\r\n')
                .replace(/<span class="emoji emoji__(\w+)" alt="(\w+)">.*?<\/span>/ig, (m, pageKey, shortcat) => {
                    let page = emoji[pageKey];
                    for (let emojiKey in page) {
                        if (page[emojiKey] === shortcat) {
                            return emojiKey;
                        }
                    }
                    
                    return m;
                });
                
            this.$textarea.val(text)
                .change()
                .focus();
        });
        
        
        let emojiHideContainerTimeout = null;
        this.$emojiButton.on('click', () => {
            this.$emojiContainer.toggleClass('is-hidden');
            this.$emojiTabs.eq(0).trigger('click');
        });
        
        let emojiFormatTemplate = `<a><span class="emoji emoji__%(pageKey)s w26" alt="%(shortcat)s">%(shortcat)s</span></a>`;
        this.$emojiTabs.on('click', ({currentTarget}) => {
            let $this = jQuery(currentTarget);
            let emojiPageKey = $this.data('page');
            if (emojiPageKey == null || lists[emojiPageKey] == null)
                return;
            
            this.$emojiTabs.removeClass('is-active');
            $this.addClass('is-active');
            
            this.$emojiList.html('');
            each(lists[emojiPageKey], key => {
                let $renderedTemplate = jQuery(sprintf(emojiFormatTemplate, {
                    'pageKey': emojiPageKey,
                    'shortcat': emoji[emojiPageKey][key]
                }));
                $renderedTemplate.get(0).__ic_key = key;
                
                $renderedTemplate.appendTo(this.$emojiList);
            });
        });
        
        this.$emojiList.on('click', 'a', (e) => {
            this.$textarea.focus();
            let message = this.$textarea.val();
            let textarea = this.$textarea.get(0);
            let {selectionStart, selectionEnd} = textarea;
            
            message = message.slice(0, selectionStart)
                + e.currentTarget.__ic_key
                + message.slice(selectionEnd);
                
            let caretPosition = selectionStart + e.currentTarget.__ic_key.length;
            
            this.$textarea.val(message);
            textarea.selectionStart = textarea.selectionEnd = caretPosition;
            
            e.preventDefault();
            return false;
        });
        
        this.$emojiContainer.on({
            'mouseover': () => clearTimeout(emojiHideContainerTimeout),
            'mouseout': (e) => {
                if (this.$emojiContainer.hasClass('is-hidden'))
                    return;
                
                emojiHideContainerTimeout = setTimeout(() => {
                    this.$emojiContainer.addClass('is-hidden');
                }, 700);
            }
        });
    }

    /**
     * Обработчик события, выполняемый при открытии активности с месенджером
     */
    onOpen() {
        if (this.activeRoom) {
            this.imageUploader.files = this.activeRoom._files || [];
            this.imageUploader.renderPreviews(null, true);
            this.activeRoom.render();
            this.$messageList.scrollTop(this.$messageList[0].scrollHeight);
        }
    }
    
    /**
     * Обработчик события, при закрытии активности месенджера
     */
    onClose() {
        if (this.activeRoom) {
            this.activeRoom._files = this.imageUploader.files;
            this.unselectMessages();
            this.$messageList.html('');
        }
    }
    
    /**
     * Обработчик события, после отправки собщения
     */
    onAfterSend() {
        this.$roomBody.removeClass('has-preview');
        this.$textarea.val('').trigger('focus');
        this.activeRoom.images = [];
        this.imageUploader.files = [];
        this.imageUploader.renderPreviews(null, true);
        this.$messageList.scrollTop(this.$messageList[0].scrollHeight);
    }
    
    /**
     * Смена комнаты в активности месенджера
     * 
     * @param {ObjectId} roomID    id модели Room
     */
    changeRoom(roomId, isVirtual) {
        this.onClose();
        this.$root.removeClass('activity--empty');
        this.rooms.changeRoom(roomId);
        this.imageUploader.config.url = `/upload/?rid=${this.activeRoom.rid}`;
        
        if (this.isOpened) {
            this.onOpen();
        } else {
            this.open();
        }
    }
    
    /**
     * Создает сообщение и отрисовывает в активной комнате
     * 
     * @param {object} data             Информация о сообщении
     * @param {string} channelName      Название канала
     */
    createMessage(data, channelName) {
        if (data.user && data.user.id) {
            data.user = new User(data.user);
        }
        
        let room = this.rooms.get(data.rid, true);
        let needAutoScroll = room.isActiveRoom
            && this.$messageList.scrollTop() === (
                this.$messageList[0].scrollHeight - this.$messageList.innerHeight());
        
        let message = room.createMessage(data, channelName);
        
        if (!needAutoScroll) {
            message.isNotReaded = true;
            return;
        }
        
        this.$messageList.scrollTop(this.$messageList[0].scrollHeight);
    }
    
    /**
     * Создает комнату
     * 
     * @param {object} data         Информация о комнате
     * @param {ObjectId} data.rid   ID модели комнаты
     * 
     * @return {Room} Возвращает модель комнаты
     */
    addRoom({rid, messages}) {
        let room = this.rooms.get(rid, true);
        eachRight(room.messages, message => message.remove());
        
        each(messages, message => {
            this.createMessage(message);
        });
        
        return room;
    }
    
    /**
     * Возвращает выделенные временные сообщения
     */
    getSelectedTempMessages() {
        return this.selectedMessages.filter(({data}) => data.removeTime);
    }
    
    /**
     * Меняет состояние выделения сообщения и меняет шапку активности
     */
    changeMessageSelectState() {
        this.$actionsHeaderTitle.text(this.selectedMessages.length);
        
        if (this.selectedMessages.length) {
            this.$actionsHeader.addClass('is-opened');
            
            let _method = (trueCondition) => {
                return `${trueCondition ? 'remove' : 'add'}Class`;
            };
            
            let tempMessageCount = this.getSelectedTempMessages().length;
            this.$actionSaveButton[_method(tempMessageCount)]('is-hidden');
            this.$actionEditButton[_method(
                this.selectedMessages.length === 1
                    && !this.selectedMessages[0].data.removeTime
                    && this.selectedMessages[0].data.owner
                )]('is-hidden');
        } else {
            this.$actionsHeader.removeClass('is-opened');
        }
    }
    
    /**
     * Снимает выделение со всех выделеных сообщений
     */
    unselectMessages() {
        eachRight(this.selectedMessages, msg => msg.changeSelectState());
    }
}


Activity.Classes['messager'] = MessagerActivity;


export {MessagerActivity as default};
