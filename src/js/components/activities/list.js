import jQuery from 'jQuery';
import Activity from './base';


/**
 * @class Активность со список всех комнат
 * @extends Activity
 */
class ListActivity extends Activity {
    /**
     * Создает представление активности со списком комнат
     */
    constructor(node, name, url) {
        super(node, name, url);
        this.$header = this.$root.find('.activity__header');
        this.$avatar = this.$header.find('.activity__avatar');
        this.$list = this.$root.find('.list');
        this.items = [];
        this.itemTemplate = null;
        
        this._bindEvents();
    }
    
    /**
     * Добавление обработчиков на события активности
     * @protected
     */
    _bindEvents() {
        this.$list.on('click', '>li', ({currentTarget}) => {
            this.onItemClick(jQuery(currentTarget));
        });
    }
    
    /**
     * Обработчиков события нажатия на элемент списка
     * 
     * @param {jQuery} $item    Элемент списка на которое совершен клик
     */
    onItemClick($item) {}
    
    /**
     * Поиск элемента списка по селектору
     * 
     * @param {string} selector     Селектор для поиска
     * 
     * @return {jQuery} Элемент комнаты
     */
    findItem(selector) {
        return this.$list.find(selector);
    }
    
    /**
     * Добавляет элемент к списку
     * 
     * @param {object} locals       Переменные для рендеринга шаблона
     */
    addItem(locals) {
        if (!this.itemTemplate) {
            console.warn('Необходимо указать шаблон элемента списка');
            return;
        }
        
        jQuery(this.itemTemplate(locals)).appendTo(this.$list);
    }
    
    /**
     * Удаляет элемент списка по индексу
     */
    removeItem(index) {
        this.$list.children().eq(index).remove();
    }
    
    /**
     * Очищает список от всех элементов
     */
    clear() {
        this.$list.html('');
    }
}


Activity.Classes['list'] = ListActivity;


export {ListActivity as default};