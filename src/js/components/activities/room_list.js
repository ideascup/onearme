import Activity from './base';
import ListActivity from './list';
import app from 'components/app';

import itemTemplate from './templates/room_list.pug';


/**
 * @class Активности со списком комнат
 * @extends ListActivity
 */
class RoomListActivity extends ListActivity {
    /**
     * Создает представление активности со списком комнат
     */
    constructor(node, name, url) {
        super(node, name, url);
        this.roomIds = [];
        this.itemTemplate = itemTemplate;
    }
    
    /**
     * @inheritdoc
     */
    onItemClick($item) {
        app.messager.changeRoom($item.data('rid'));
    }
    
    /**
     * Поиск элемента списка по id комнаты
     * 
     * @param {ObjectId} roomId     id модели комнаты
     */
    findItemById(roomId) {
        return this.findItem(`[data-rid="${roomId}"]`);
    }
    
    /**
     * Добавление элемента комнаты в список
     * 
     * @param {Room} room       Модель комнаты
     * 
     * @override
     */
    addItem(room) {
        const roomId = room.rid;
        if (!roomId || this.roomIds.indexOf(roomId) > -1)
            return;
        
        this.roomIds.push(roomId);
        super.addItem(room.locals);
    }
    
    /**
     * Удаление элемента комнаты из списка
     * 
     * @param {ObjectId} roomId     id модели комнаты
     * 
     * @override
     */
    removeItem(roomId) {
        const index = this.roomIds.indexOf(roomId);
        if (index === -1)
            return;
            
        let activeRoom = app.messager.activeRoom;
        if (activeRoom && roomId === activeRoom.rid) {
            activeRoom.close();
        }
        
        this.roomIds.splice(index, 1);
        this.findItemById(roomId).remove();
    }
    
    /**
     * @inheritdoc
     */
    clear() {
        super.clear();
        this.roomIds = [];
    }
}


Activity.Classes['list:rooms'] = RoomListActivity;


export {RoomListActivity as default};