import jQuery from 'jQuery';
import each from 'lodash/each';
import Daemon from 'daemon';
import Channels from 'channels';
import ICNotification from 'ic/notification/base';
import WebSocketClient from 'components/websocket_client';
import {Activity} from 'components/activities/all';
import Sidebar from 'ic/sidebars/base';
import wscEvents from 'wsc_events/_all';


let wsc = null;

let __initialized = false;

/**
 * @namespace app
 */
let app = {
    /** @type {User} */
    currentUser: null,
    
    /** @type {RoomListActivity} */
    roomList: null,
    
    /** @type {UserListActivity} */
    userList: null,
    
    /** @type {MessagerActivity} */
    messager: null,
    
    /**
     * @type {Daemon}
     * @protected
     */
    _daemon: null,
    
    /**
     * @type {bool}
     * @protected
     */
    _isActiveBrowserTab: true,
    
    /**
     * Инициализация чата
     */
    initialize() {
        if (__initialized) return;
        __initialized = true;
        
        wsc = WebSocketClient();
        ICNotification.requestPermission();
        
        Sidebar.initialize();
        
        Activity.initialize();
        this.roomList = Activity.getByName('room_list');
        this.userList = Activity.getByName('user_list');
        this.messager = Activity.getByName('messager');
        
        this._daemon = new Daemon({delay: 100}).start();
        
        if (this.messager) {
            this._bindEvents();
        }
        
        jQuery('#join_room_activity form').on('ra.form.success', (e, data) => {
            window.location = '/';
        });
    },
    
    /**
     * Добавление обработчиков на события
     * @protected
     */
    _bindEvents() {
        jQuery(window).on({
            'focus': () => this._isActiveBrowserTab = true,
            'blur': () => this._isActiveBrowserTab = false,
            'resize': () => this.messager.isLocked = window.innerWidth >= 800
        }).resize();
        
        jQuery('#add_room_activity form').on('ra.form.success', (e, data) => {
            wsc.send(Channels.JOIN, data);
            Activity.getOpened().close();
        });
        
        jQuery('[ic-activity="user_signin"],[ic-activity="user_signup"]')
            .find('form').on('ra.form.success', (e, data) => {
                this.roomList.open();
                wsc.connect();
            });
        
        each(wscEvents, (initHandler, channelName) => {
            wsc.on(channelName, initHandler.call(this, wsc));
        });
        
        this._daemon.onTick(() => {
            const messager = this.messager;
            const _now = Date.now();
            
            each(messager.rooms.getAllMessages(), message => {
                if (message.data.removeTime && message.data.removeTime < _now) {
                    message.remove();
                }
                
                message.onTick();
            });
            
            each(messager.rooms.getList(), (room) => {
                let $listItem = this.roomList.findItemById(room.rid);
                let $removeTime = $listItem.find('.js-room-remove-time');
                
                if (room.lifetime) {
                    let removeTimeMinutes = room.getRemoteTimeMinutes();
                    
                    if (+$removeTime.text() !== removeTimeMinutes) {
                        $removeTime.text(removeTimeMinutes);
                        if (app.messager.rooms.activeId === room.rid) {
                            app.messager.$roomHeader.find('.js-room-remove-time')
                                .text(removeTimeMinutes);
                        }
                    }
                } else {
                    $removeTime.addClass('is-hidden');
                    app.messager.$roomHeader.find('.js-room-remove-time').addClass('is-hidden');
                }
            });
        });
    }
};


export default app;