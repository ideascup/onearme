import jQuery from 'jQuery';


const DEFAULT_FIELD_CONFIG = {
    required: false,
    pattern: null,
    min: -1,
    max: -1,
    equalTo: null
};

const State = {
    HIDDEN: 'is-hidden',
    ERROR: 'is-invalided',
    EMPTY: 'is-empty',
    FOCUSED: 'is-focused'
};

const Notify = {
    SUCCESS: 'c-notify--success',
    ERROR: 'c-notify--error'
};


class FormField {
    constructor(root, raform) {
        this.$root = jQuery(root);
        this.$field = this.$root.closest('.field');
        this.$label = this.$field.find('label');
        this.raform = raform;
        this.config = jQuery.extend({}, DEFAULT_FIELD_CONFIG, this.$root.data());
        this.name = '';
        this.type = '';
        this._re = null;
        this._equalItemIndex = null;
        this._initialize();
    }
    
    _initialize() {
        this.name = this.$root[0].name;
        this.type = this.$root.attr('type');
        
        if (this.config.equalTo) {
            this._equalItemIndex = this.raform.$inputs
                .filter(`[name="${this.config.equalTo}"]`)
                .index();
        }
        
        const _pattern = this.config.pattern;
        if (_pattern) {
            const m = /\/[gim]{1,3}$/.exec(_pattern);
            const parts = m ?
                [_pattern.slice(0, m.index), _pattern.slice(m.index + 1)]
                : [_pattern, ''];
            this._re = new RegExp(`^${parts[0]}$`, parts[1]);
        }
        
        this._bindEvents();
    }
    
    _bindEvents() {
        this.$root.on({
            'focus': () => {
                this.$field.addClass(State.FOCUSED);
            },
            'blur': () => {
                this.$field.removeClass(State.FOCUSED);
                this.validate();
            },
            'focus input change': () => {
                let _value = this.$root.val();
                if (this.type !== 'password') {
                    _value = _value.trim();
                }
                
                if (this.config.max > -1 && _value.length > this.config.max) {
                    _value = _value.slice(0, this.config.max);
                    this.$root.val(_value);
                }
                
                this.raform.data[this.name] = _value;
                this.raform.validate();   
            }
        });
    }
    
    validate(silence) {
        const _value = this.$root.val();
        let error = false;
        
        if (this._equalItemIndex) {
            error = this.raform.fields[this._equalItemIndex].validate(silence);
        } else {
            error = (!_value.length && this.config.required) 
                || (this._re && !this._re.test(_value))
                || (this.config.min > -1 && _value.length < this.config.min);
        }        
        
        if (error) {
            !silence && this.$root.addClass(State.ERROR);
        } else {
            this.$root.removeClass(State.ERROR);
        }
        
        let method = `${_value.trim() ? 'remove' : 'add'}Class`;
        this.$field[method](State.EMPTY);
        
        return error;
    }
}


const DEFAULT_VALIDATOR_CONFIG = {
    ajax: false
};


class FormValidator {
    constructor(root, config) {
        this.$root = jQuery(root);
        this.$submit = this.$root.find('[type=submit]');
        this.$inputs = this.$root.find('input[name]:not([type=submit]),textarea');
        this.$notify = this.$root.find('.c-notify');
        this.fields = [].map.call(this.$inputs, (el) => new FormField(el, this));
        this.config = jQuery.extend({}, DEFAULT_VALIDATOR_CONFIG, config, this.$root.data());
        this.data = {};
        this._initialize();
    }
    
    _initialize() {
        this.$inputs.each((i, el) => this.data[el.name] = el.value);
        this.validate();
        this._bindEvents();
    }
    
    _bindEvents() {
        this.$root.on('submit', (evt) => this.send(evt));
    }
    
    validate() {
        let error = false;
        this.fields.forEach(field => field.validate(true) && (error = true));
        this.$submit.prop('disabled', error);
        return !error;
    }
    
    send(evt) {
        if (!this.validate()) {
            evt.preventDefault();
            return false;
        }
        
        this.$submit.prop('disabled', true);
        this.$notify.addClass([State.HIDDEN, Notify.SUCCESS].join(' '))
            .removeClass(Notify.ERROR).html('');
            
        if (this.config.ajax) {
            const _form = this.$root[0];
            jQuery.ajax({
                method: _form.method,
                url: _form.action,
                data: this.data
            }).done((data) => {
                data || (data = {});
                if (data['success']) {
                    if (data['message']) {
                        this.$notify.removeClass(State.HIDDEN).html(data.message);
                    }
                    this.$inputs.each((i, el) => el.value = '');
                    this.$root.trigger('ra.form.success', data);
                    this.data = {};
                } else if (data['errors']) {
                    const _errors = data['errors'];
                    
                    jQuery.each(this.fields, (i, field) => {
                        const _errorMessages = _errors[field.name];
                        const $errors = field.$root.parent().find('.field__errors').html('');
                      
                        if (_errorMessages) {
                            field.validate();
                            $errors.append(`<li>${_errorMessages[0]}</li>`);
                        }
                    });
                    
                }
        
                this.validate();
            }).fail(() => {
                this.$notify.removeClass([State.HIDDEN, Notify.Success].join(' '))
                    .addClass(Notify.ERROR)
                    .html('Что-то пошло не так. Попробуйте позже.');
                this.$submit.prop('disabled', false);
            });
            
            evt.preventDefault();
            return false;
        }
    }
}


jQuery.fn.raform = function (config) {
    return this.each(function () {
        new FormValidator(this, config);
    });
};


jQuery(() => {
    jQuery('[data-raform]').raform().removeAttr('data-raform');
});
