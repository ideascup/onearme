import Message from './base';
import User from 'models/user';

import liveMessageTemplate from './templates/message_live.pug';


/**
 * @class Объект 'живого' сообщения пользователя при вводе текста
 * @extends Message
 */
export class LiveMessage extends Message {
    constructor(room, messageData) {
        super(room, messageData);
        this.template = liveMessageTemplate;
        this.templateSelector = '.messager--typing';
        
        this.data.avatarSize = 20;
    }
    
    /**
     * Добавление или обновление существуещего сообщения в коллекции
     * @protected
     */
    _add() {
        let messages = this._room.messages;
        
        const founded = messages.length && messages.some((msg, i) => {
            if (msg instanceof LiveMessage
                    && msg.data.avatar === this.data.avatar) {
                messages.splice(i, 0, this);
                msg.remove();
                return true;
            }
            
            return false;
        });
        
        if (!founded) {
            super._add();
        }
    }
}


export {LiveMessage as default};