import filter from 'lodash/filter';
import LiveMessage from './live';

import liveMessageTemplate from './templates/message_live.pug';


/**
 * @class Объект сообщения пользователя при вводе текста
 * @extends LiveMessage
 */
class TypingMessage extends LiveMessage {
    constructor(room, messageData, wrapper) {
        super(room, messageData);
        this.typingWrapper = wrapper;
        this.template = liveMessageTemplate;
        this.templateMethod = 'insertBefore';
        this.$templateTargets = wrapper.$element && wrapper.$element.find('span').eq(0);
        this.autoscroll = false;
    }
    
    render() {
        if (!this.$templateTargets)
            return;
        
        super.render();
    }
    
    /**
     * Удаляет сообщение из коллекции а так же удаляет обертку для сообщения
     * если это сообщение последнее
     */
    remove() {
        if (this.getTypingMessages().length === 1) {
            this.typingWrapper.remove();
        }
        
        super.remove();
    }
    
    getTypingMessages() {
        return filter(this._room.messages, msg => msg instanceof TypingMessage);
    }
}


export {TypingMessage as default};