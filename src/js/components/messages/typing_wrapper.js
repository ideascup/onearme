import filter from 'lodash/filter';
import app from 'components/app';
import Message from './base';

import liveMessageTemplate from './templates/message_live.pug';


/**
 * @class Объект обертки для {@link TypingMessage}
 * @extends Message
 */
class TypingWrapperMessage extends Message {
    constructor(room) {
        super(room, {});
        this.template = liveMessageTemplate;
        this.templateMethod = 'appendTo';
        this.$templateTargets = app.messager.$messageList;
        this._dots = 1;
        this._tickCount = 0;
    }
    
    /**
     * Обработчик события тика демона
     */
    onTick() {
        if (!this.$element || !this.$element.length || ++this._tickCount % 4)
            return;
        
        let $elements = this.$element.find('span');
        const dots = new Array(this._dots).join('.');
        
        $elements.eq(0).text(dots);
        
        if (this._dots++ > 3) {
            this._dots = 1;
        }
    }
    
    /**
     * Получение или создание новой обертки для typing сообщений
     * 
     * @param {Room} room       Комната чата
     * @return {TypingWrapperMessage}   Возвращает обертку для typing сообщений
     */
    static get(room) {
        let [wrapper] = filter(room.messages, msg => msg instanceof this);
        if (!wrapper) {
            wrapper = new TypingWrapperMessage(room);
            if (room.rid === app.messager.rooms.activeId) {
                wrapper.render();
            }
        }
        return wrapper;
    }
}


export {TypingWrapperMessage as default};