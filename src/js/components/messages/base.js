import jQuery from 'jQuery';
import assign from 'lodash/assign';
import reduce from 'lodash/reduce';
import each from 'lodash/each';
import app from 'components/app';
import moment from 'moment';
import {emoji} from 'components/emoji';
import {sprintf} from 'sprintf-js';
        

import messageTemplate from './templates/message.pug';

/**
 * @class Объект сообщения для чата
 */
export class Message {
    /**
     * Создает представление сообщения
     * 
     * @param {object} messageData      Ответ, полученный по сокету
     */
    constructor(room, messageData) {
        this.$element = null;
        this.data = messageData;
        this._room = room;
        
        this.template = messageTemplate;
        this.templateSelector = '.message--typing';
        this.$templateTargets = null;
        this.templateMethod = null;
        
        this.isSelected = false;
        this.isReaded = true;
        this.autoscroll = true;
        this._add();
    }
    
    get lifetime() {
        return Math.ceil((this.data.removeTime - Date.now()) / 1000);
    }
    
    get offsetTop() {
        let messages = this._room.messages;
        let topMessages = messages.slice(0, messages.indexOf(this));
        topMessages.push(this);
        
        return reduce(topMessages, (result, {$element}) => {
            return result + $element.outerHeight(true);
        }, 0);
    }
    
    get isNotReaded() {
        return !this.isReaded;
    }
    
    set isNotReaded(isNotReaded) {
        if (!this.autoscroll)
            return;
        
        this.isReaded = !isNotReaded;
        if (!this._room.isActiveRoom)
            return;
            
        let $unreadButton = app.messager.$unreadButton;
        let unreadMessages = this._room.unreadMessages;
        
        if (unreadMessages.length) {
            $unreadButton.text(unreadMessages.length)
                .removeClass('is-hidden');
        } else {
            $unreadButton.addClass('is-hidden');
        }
        
    }
    
    /**
     * Добавление обработчиков на события сообщений
     * @protected
     */
    _bindEvents() {
        if (this.constructor.name !== Message.prototype.constructor.name)
            return;
            
        let _holdStartAt = null;
        let _isDisabledClickHandler = false;
        
        this.$element.on({
            'click': () => {
                if (app.messager.isEditMessage
                        || !app.messager.selectedMessages.length
                        || _isDisabledClickHandler)
                    return;
                
                this.changeSelectState();
            },
            'mousedown touchstart': () => {
                if (app.messager.selectedMessages.length
                        || _holdStartAt)
                    return;
                
                _holdStartAt = Date.now();
                
                setTimeout(() => {
                    if (_holdStartAt === null)
                        return;
                    
                    _isDisabledClickHandler = true;
                    this.changeSelectState();
                }, 500);
            },
            'mouseup touchend': () => {
                _holdStartAt = null;
                
                setTimeout(() => _isDisabledClickHandler = false, 100);
            }
        });
    }
    
    /**
     * Добавление сообщения в коллецию
     * @protected
     */
    _add() {
        this._room.messages.push(this);
    }
    
    /**
     * Удаляет сообщение из комнаты и очищает элементы
     */
    remove() {
        let messages = this._room.messages;
        messages.splice(messages.indexOf(this), 1);
        
        if (this.isSelected)
            this.changeSelectState();
        
        this.clear();
    }
    
    /**
     * Удаляет элементы, связанные с данным сообщением
     */
    clear() {
        if (!this.$element)
            return;
        
        let $prevElement = this.$element.prev();
        if ($prevElement.hasClass('message--date')) {
            $prevElement.remove();
        }
        
        this.$element.remove();
    }
    
    /**
     * Возвращает переменные, необходимые для рендеринга шаблона
     */
    getLocals() {
        if (this.data.text) {
            let formatTemplate = `<span class="emoji emoji__%(pageKey)s" alt="%(shortcat)s">%(shortcat)s</span>`;
            
            each(emoji, (page, pageKey) => {
                each(page, (shortcat, key) => {
                    if (this.data.text.indexOf(key) > -1) {
                        this.data.text = this.data.text.replace(
                            new RegExp(key, 'g'),
                            sprintf(formatTemplate, {
                                'pageKey': pageKey,
                                'shortcat': shortcat
                            }));
                    }
                });
            });
        }
        
        return {
            'data': this.data,
            'lifetime': this.lifetime,
            'createAt': moment(this.data.createAt).format('H:mm:ss')
        };
    }
    
    /**
     * Отрисовывает дату написания сообщения
     */
    _renderDate() {
        if (!this._room.isActiveRoom || !this.$element)
            return;
        
        this.$element.before(this.template({
            'data': {
                'type': 'date',
                'date': moment(this.data.createAt).format('DD / MM / Y')
            }
        }));
    }
    
    /**
     * Отрисовывает сообщение
     */
    render() {
        if (!this._room.isActiveRoom
                || (this.$element && !this.$element.length))
            return;
        
        this.$element = jQuery(this.template(this.getLocals()));
        
        let $targets = this.$templateTargets
            || (this.templateSelector && jQuery(this.templateSelector));
        let method = this.templateMethod;
        
        if ($targets && $targets.length) {
            method || (method = 'insertBefore');
        } else {
            method || (method = 'appendTo');
            $targets = app.messager.$messageList;
        }
        
        this.$element[method]($targets.eq(0) || app.messager.$messageList);
        
        let messages = this._room.messages;
        let messageIndex = messages.indexOf(this);
        if (messageIndex > 0) {
            let prevMessage = messages[messageIndex - 1];
            if (moment(prevMessage.data.createAt).isBefore(this.data.createAt, 'day'))   {
                this._renderDate();
            }
        } else if (moment().isAfter(this.data.createAt, 'day')) {
            this._renderDate();
        }
        
        this._bindEvents();
    }
    
    /**
     * Редактирует сообщение и рендерит с изменениями
     * 
     * @param {object} data     Новые данные о сообщении
     */
    editAndRender(data) {
        if (!this._room.isActiveRoom || !this.$element || !this.$element.length) 
            return;
        
        data.owner = data.user.uid === app.currentUser.uid;
        this.data = data;
        
        let $parent = this.$element.parent();
        this.$element.get(0).outerHTML = this.template(this.getLocals());
        
        this.$element = $parent.find(`[data-mid="${data.id}"]`);
        this._bindEvents();
    }
    
    /**
     * Изменяет состояние выделения сообщения
     */
    changeSelectState() {
        let {selectedMessages} = app.messager;
        if (this.isSelected) {
            selectedMessages.splice(
                selectedMessages.indexOf(this), 1);
        } else {
            selectedMessages.push(this);
        }
        
        app.messager.changeMessageSelectState();
        this.isSelected = !this.isSelected;
        this.$element[`${this.isSelected ? 'add' : 'remove'}Class`]('is-selected');
    }
    
    /**
     * Обработчик события тика демона
     */
    onTick() {
        if (!this.$element || !this.$element.length)
            return;
        
        // todo: Нужно проверять отдельно типы
        if (this.data.removeTime) {
            this.$element.find('.trash').text(this.lifetime);
        }
    }
}


export {Message as default};