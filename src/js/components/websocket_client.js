import each from 'lodash/each';
import Channels from 'channels';
import eioClient from 'engine.io-client';
import Daemon from 'daemon';
import app from 'components/app';

let __instance = null;

/**
 * @class Клиент для вебсокета
 */
class WSClient {
    /**
     * Создает клиент для вебсокета
     *       
     * @param {object} config   Настройки engine.io-client
     */
    constructor(config) {
        if (__instance) return __instance;
        __instance = this;
        
        this.config = config;
        this._wsc = null;
        this._events = {};
        this._channels = {};
        this._daemon = new Daemon({delay: 1500}).start();
        
        this._bindEvents();
    }
    
    /**
     * Добавление обработчиков на события вебсокета
     * @protected
     */
    _bindEvents() {
        this._daemon.onTick(() => {
            if (this._wsc
                    && this._wsc.readyState === 'closed'
                    && app._isActiveBrowserTab) {
                this._wsc.open();
            }
        });
    }
    
    /**
     * Соединяет с вебсокет сервером
     * 
     * @return {WSClient}
     */
    connect() {
        if (this._wsc && this._wsc.readyState !== 'closed') {
            this._wsc.close();
        }
        
        this._wsc = new eioClient(this.config);
        
        this._wsc.on('open', () => {
            this.send(Channels.CONNECT, {});
        });
        
        this._wsc.on('message', (data) => {
            try {
                data = JSON.parse(data);
            } catch(e) {
                data = {};
            }
            
            const channelName = data.channel;
            if (!channelName)
                return;
                
            delete data.channel;
            
            const handlers = this._channels[channelName];
            handlers && each(handlers, fn => fn(data));
        });
        
        this._wsc.on('close', (e) => {
            this._wsc = null;
        });
        
        return this;
    }
    
    /**
     * Добавляет обработчик на канал
     * 
     * @param {string} channelName      Название канала
     * @param {Function} handler        Обработчик события для канала
     * @return {WSClient}
     */
    on(channelName, handler) {
        if (typeof handler !== 'function' || !this._wsc)
            return this;
        this._channels[channelName] || (this._channels[channelName] = []);
        this._channels[channelName].push(handler);
        return this;
    }

    /**
     * Отправляет сообщение на сервер
     * 
     * @param {string} channelName      Название канала
     * @param {object|string} data      Сообщение
     * 
     * @return {bool} Ошибка отправки сообщения
     */
    send(channelName, data) {
        if (!this._wsc || this._wsc.readyState !== 'open')
            return true;
            
        if (typeof data === 'string') {
            data = {text: data};
        }
        
        data.channel = channelName;
        this._wsc.send(JSON.stringify(data));
        return false;
    }
}


export default function (config) {
    return new WSClient(config);
}