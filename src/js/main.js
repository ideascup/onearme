/*global location*/
import WebSocketClient from 'components/websocket_client';
import app from 'components/app';

import 'components/raform';
import RangeField from 'ic/fields/range';


const wss = WebSocketClient({
    host: 'ws.' + location.hostname,
    hostname: 'ws.' + location.hostname,
    path: '/',
    port: location.protocol === 'https' ? '443' : null,
    secure: location.protocol === 'https'
});

wss.connect();

app.initialize();
RangeField.initialize();