import each from 'lodash/each';
import filter from 'lodash/filter';
import sortBy from 'lodash/sortBy';
import Channels from 'channels';
import app from 'components/app';
import Message from 'components/messages/base';
import LiveMessage from 'components/messages/live';
import TypingMessage from 'components/messages/typing';
import TypingWrapperMessage from 'components/messages/typing_wrapper';

import itemInfoTemplate from 'rooms_list_info.pug';
import itemAvatarTemplate from 'components/activities/templates/avatar.pug';


/**
 * @class Хранит полную пользовательскую информацию о созданной в чате комнате
 */
class Room {
    /**
     * Создает представление модели комнаты
     * 
     * @param {ObjectId} roomId     id модели Room
     */
    constructor(roomId) {
        /** @type {ObjectId} */
        this.rid = roomId;
        /** @enum {Message} */
        this.messages = [];
        /** @enum {Feature} */
        this.features = [];
        /** @enum {string} */
        this.images = [];
        /** @type {Array.<Object>} */
        this.users = [];
        /** @type {number} */
        this.removeTime = 0;
        /** @type {boolean} */
        this.isDialog = false;
        /** @type {string} */
        this._name = null;
        /** @type {object} */
        this._avatarInfo = null;
        /** @type {number} */
        this._lifetime = null;
        /** @type {number} */
        this._messageLifetime = null;
    }
    
    /**
     * __Getter__
     * Возвращает название комнаты
     * 
     * @type {string}
     */
    get name() {
        return this._name;
    }
    
    /**
     * __Setter__
     * Задает название для комнаты
     * 
     * @param {string} name     Новое значение названия комнаты
     */
    set name(name) {
        this._name = name;
        
        this.getRoomItem().find('h2').text(name);
        if (this.isActiveRoom) {
            app.messager.$roomTitle.text(name);
        }
    }
    
    /**
     * __Getter__
     * Возвращает информацию об аватаре комнаты
     * 
     * @type {object}
     */
    get avatarInfo() {
        return this._avatarInfo;
    }
    
    /**
     * __Setter__
     * Задает информацию об аватаре комнаты
     * 
     * @param {object} avatarInfo   Информация об аватаре комнаты
     */
    set avatarInfo(avatarInfo) {
        this._avatarInfo = avatarInfo;
        
        if (this.isActiveRoom) {
            app.messager.$roomIcon.html(itemAvatarTemplate(this.locals));
        }
    }
    
    /**
     * __Getter__
     * Возвращает время жизни комнаты
     * 
     * @type {number}
     */
    get lifetime() {
        return this._lifetime;
    }
    
    /**
     * __Setter__
     * Задает время жизни комнаты
     * 
     * @param {number} lifetime     Новое значение времени жизни комнаты
     */
    set lifetime(lifetime) {
        this._lifetime = lifetime;
    }
    
    /**
     * __Getter__
     * Возвращает время жизни сообщения
     * 
     * @type {number} 
     */
    get messageLifetime() {
        return this._messageLifetime;
    }
    
    /**
     * __Setter__
     * Задает время жищни для сообщений комнаты
     * 
     * @param {number} messageLifetime  Новое значение времени жизни сообщения
     */
    set messageLifetime(messageLifetime) {
        this._messageLifetime = messageLifetime;
    }
    
    /**
     * __Getter__
     * Является ли данная комната активной
     * 
     * @type {boolean}
     */
    get isActiveRoom() {
        return this.rid === app.messager.rooms.activeId;
    }
    
    /**
     * __Getter__
     * Возвращает непрочитанные сообщения
     * 
     * Фильтрует все сообщения по отрицательному параметру `isNotReaded`
     * в объекте типа Room.
     * 
     * @enum {Message}
     */
    get unreadMessages() {
        return filter(this.messages, 'isNotReaded');
    }
    
    /**
     * __Getter__
     * Возвращает объект переменных для внутреннего использования в шаблонах
     * 
     * @type {object}
     */
    get locals() {
        return {
            'rid': this.rid,
            'name': this.name,
            'avatarInfo': this.avatarInfo,
            'lifetime': this.lifetime,
            'message_lifetime': this.messageLifetime
        };
    }
    
    /**
     * __Getter__
     * Возвращает изменяемые настройки комнаты
     * 
     * @type {object}
     */
    get preferences() {
        return {
            'name': this._name,
            'lifetime': this._lifetime,
            'message_lifetime': this._messageLifetime
        };
    }
    
    /**
     * Возвращает итем списка комнат
     * 
     * @return {jQuery} Текущий итем комнаты
     */
    getRoomItem() {
        return app.roomList.findItemById(this.rid);
    }
    
    /**
     * Изменяет информацию о комнате
     * 
     * @param {object} data
     */
    changeInfo({removeTime, users, onlineUids, info}) {
        removeTime && (this.removeTime = removeTime);
        onlineUids && (this.onlineUids = onlineUids);
        users && (this.users = users);
        
        if (info) {
            each(info, (value, key) => {
                if (key in this) {
                    this[key] = value;
                }
            });
        }
        
        this.renderAdditionalInfo();
    }
    
    /**
     * Отрисовывает шаблон информации о комнате
     * 
     * @params {object} [locals]  Переменные, которые необходимо отрисовать
     */
    renderInfo(locals) {
        if (!this.isActiveRoom)
            return;
        
        if (!locals)
            return this.renderInfo(this.locals);
        
        if ('name' in locals) {
            app.messager.$roomTitle.text(locals.name);
        }
        
        if ('avatarInfo' in locals) {
            app.messager.$roomIcon.html(itemAvatarTemplate(this.locals));
        }
        
        if ('lifetime' in locals) {
            app.messager.$roomHeader.find('.js-room-remove-time')
                .text(locals.lifetime);
        }
    }
    
    /**
     * Отрисовывает шаблон дополнительной информации о комнате
     */
    renderAdditionalInfo() {
        let renderedInfo = itemInfoTemplate({
            'currentUser': app.currentUser,
            'users': this.users,
            'onlineUids': this.onlineUids,
            'removeTimeMinutes': this.getRemoteTimeMinutes(),
            'isDialog': this.isDialog
        });
        
        let $listItem = app.roomList.findItemById(this.rid);
        if ($listItem.length) {
            $listItem.find('.room-info').html(renderedInfo);
            $listItem.find('.list__avatar').html(itemAvatarTemplate(this.locals));
        }
        
        if (this.isActiveRoom) {
            app.messager.$roomHeader.find('.room-info').html(renderedInfo);
        }
    }
    
    /**
     * Возвращает время в минутах, через которое удалится комната
     * 
     * @return {number}     Время удаления комнаты в минутах
     */
    getRemoteTimeMinutes() {
        return Math.ceil((this.removeTime - Date.now()) / 1000 / 60);
    }
    
    /**
     * Создает сообщение в комнате по названию канала из которого пришло
     * 
     * @param {object} data         Информация о сообщении с сервера
     * @param {string} channelName  Название канала чата
     * 
     * @return {Message}
     */
    createMessage(data, channelName) {
        let message;
        data.owner = data.user.uid === app.currentUser.uid;
        
        if (channelName === Channels.TYPING) {
            if (data.type === 'live') {
                message = new LiveMessage(this, data);
            } else {
                message = new TypingMessage(this, data, TypingWrapperMessage.get(this));
            }
        } else {
            message = new Message(this, data);
            each(this.messages, msg => {
                if (msg instanceof LiveMessage
                        && msg.data.avatar === data.avatar) {
                    msg.remove();
                }
            });
        }
        
        if (this.isActiveRoom) {
            message.render();
        }
        
        return message;
    }
    
    /**
     * Полностью очищает и заного отрисовывает элементы комнаты в порядке их
     * получения
     */
    render() {
        this.sort();
        this.clear();
        each(this.messages, msg => msg.render());
    }
    
    /**
     * Сортирует сообщения по дате создания
     */
    sort() {
        this.messages = sortBy(this.messages, [({data}) => data.createAt]);
    }
    
    /**
     * Очищает окно комнаты от сообщений
     */
    clear() {
        // each(this.messages, msg => msg.clear());
    }
    
    /**
     * Закрывает комнату
     */
    close() {
        app.messager.rooms._activeRoomId = null;
        app.messager.close();
    }
}


export {Room as default};