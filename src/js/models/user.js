let __users = {};


/**
 * @class Хранит полную информацию о пользователе
 */
class User {
    /**
     * Создает представление модели пользователя
     * 
     * @param {object} data             Информация о пользователе
     * @param {ObjectId} data.id        id модели User
     * @param {string} data.name        Имя пользователя
     * @param {object} data.avatarInfo  Информация об аватаре пользователя
     */
    constructor({id, name, avatarInfo}) {
        if (__users[id])
            return __users[id];
        __users[id] = this;
        
        /** @type {ObjectId} */
        this.uid = id;
        /** @type {string} */
        this.name = name;
        /** @type {object} */
        this.avatarInfo = avatarInfo;
    }
    
    /**
     * __Getter__
     * Возвращает объект переменных для внутреннего использования в шаблонах
     * 
     * @type {object}
     */
    get locals() {
        return {
            'uid': this.uid,
            'name': this.name,
            'avatarInfo': this.avatarInfo
        };
    }
    
    /**
     * Возвращает модель пользователя по id пользователя
     * 
     * @param {ObjectId} uid        id пользователя
     * 
     * @return {User}
     */
    static getById(uid) {
        return __users[uid];
    }
}


export {User as default};