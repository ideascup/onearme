const fs = require('fs');
const path = require('path');
const gulp = require('gulp');
const scss = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const gulplog = require('gulplog');
const gulpIf = require('gulp-if');
const del = require('del');
const newer = require('gulp-newer');
const debug = require('gulp-debug');
const imagemin = require('gulp-imagemin');
const cssnano = require('gulp-cssnano');
const uglify = require('gulp-uglify');
const combine = require('stream-combiner2').obj;
const webpackStream = require('webpack-stream');
const webpack = webpackStream.webpack;
const named = require('vinyl-named');
const buffer = require('vinyl-buffer');
const jsdoc = require('gulp-jsdoc3');
const imageResize = require('gulp-image-resize');
const spritesmith = require('gulp.spritesmith');
const concat = require('gulp-concat');
const merge = require('merge-stream');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';
const distDir = 'app/static';
const docFiles = [
    'README.md',
    'src/js/components/**/*.js',
    'app_node/**/*.js'
];

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath)
    .filter(name => fs.statSync(path.join(srcpath, name)).isDirectory());
}


// Documentation
gulp.task('clean-doc', () => del('docs'));

gulp.task('jsdoc', cb => {
    return gulp.src(docFiles, {read: false})
        .pipe(jsdoc(cb));
});

gulp.task('doc', gulp.series('clean-doc', 'jsdoc'));


// Build
gulp.task('scss', () => {
    const autoprefixerConfig = [
        'Android 2.3',
        'Android >= 4',
        'Chrome >= 20',
        'Firefox >= 24', // Firefox 24 is the latest ESR
        'Explorer >= 8',
        'iOS >= 6',
        'Opera >= 12',
        'Safari >= 6'
    ];
    
    return gulp.src('src/scss/main.scss')
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(scss()).on('error', scss.logError)
        .pipe(autoprefixer(autoprefixerConfig))
        .pipe(gulpIf(isDevelopment, sourcemaps.write()))
        .pipe(gulpIf(!isDevelopment, cssnano()))
        .pipe(gulp.dest(distDir));
});

gulp.task('assets', () => {
    return gulp.src('src/assets/**', {since: gulp.lastRun('assets')})
        .pipe(newer(distDir))
        .pipe(debug({title: 'assets'}))
        .pipe(gulpIf('*.{png,jpg,gif}', imagemin()))
        .pipe(gulp.dest(distDir));
});

gulp.task('webpack', (callback) => {
    let firstBuildReady = false;
    
    function done(err, stats) {
        firstBuildReady = true;
        if (err) return;
        
        gulplog[stats.hasErrors() ? 'error' : 'info'](stats.toString({
            colors: true
        }));
    }
    
    const options = {
        watch: isDevelopment,
        devtool: isDevelopment ? '#inline-source-map' : null,
        resolve: {
            modulesDirectories: [
                './src/js',  // компоненты
                './app_node/common',    // websocket server
                './src/templates',      // шаблоны
                path.join(__dirname, 'bower_components'),
                path.join(__dirname, 'node_modules')
            ],
            alias: {
                jQuery: 'jquery/dist/jquery.min.js',
                WebSocket: 'reconnectingWebsocket/reconnecting-websocket.min.js'
            }
        },
        module: {
            loaders: [{
                test: /\.js$/,
                include: [
                    path.join(__dirname, 'src/js'),
                    path.join(__dirname, 'app_node/common'),
                    fs.realpathSync(path.join(__dirname, 'src/js/ic'))
                ],
                loader: 'babel?cacheDirectory'
            }, {
                test: /\.pug$/,
                include: [
                    path.join(__dirname, 'src'),
                    fs.realpathSync(path.join(__dirname, 'src/js/ic'))
                ],
                loader: 'babel!pug'
            }],
            noParse: /\/(node_modules|bower_components)\/(jquery|reconnectingWebsocket)/
        },
        plugins: [
            new webpack.NoErrorsPlugin(),
            new webpack.DefinePlugin({
                'NodeEnv.isDev': JSON.stringify(isDevelopment)
            }),
            new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/)
        ]
    };
    
    return gulp.src('src/js/*.js')
        .pipe(named())
        .pipe(webpackStream(options, null, done))
        .pipe(gulpIf(!isDevelopment, uglify()))
        .pipe(gulp.dest(distDir))
        .on('data', () => {
            if (firstBuildReady) callback();
        });
});

/* Sprites */
gulp.task('sprite', () => {
    let imgStreams = [];
    let cssStreams = [];
    let imageResizeOptions = {
        'width': 64,
        'height': 64,
        'imageMagick': true
    };
    
    getDirectories('src/sprites/').forEach((dir, i) => {
        let {img, css} = gulp.src(path.join('src/sprites/', dir, '*.png'))
            .pipe(imageResize(imageResizeOptions))
            .pipe(spritesmith({
                imgPath: `/emoji/${dir}.png?_=${Date.now()}`, 
                imgName: `${dir}.png`,
                cssName: `${dir}.scss`,
                cssTemplate: 'src/sprites/scss_emoji.template.handlebars',
                cssOpts: {
                    selector: `emoji__${dir}`
                },
                cssVarMap(sprite) {
                    sprite.name = sprite.name.replace(/-/g, '_');
                }
            }));
        
        imgStreams.push(img);
        cssStreams.push(css);
    });
    
    return merge(
        merge.apply(null, imgStreams)
            .pipe(buffer())
            .pipe(imagemin())
            .pipe(gulp.dest('src/assets/emoji')),
        merge.apply(null, cssStreams)
            .pipe(concat('_emoji.scss'))
            .pipe(gulp.dest('src/scss/sprites')));
});


gulp.task('clean', () => del(distDir));

gulp.task('watch', () => {
    gulp.watch([
        'src/scss/**/*.scss',
        fs.realpathSync(path.join(__dirname, 'src/scss/ic')) + '/**/*.scss'
    ], gulp.series('scss'));
    gulp.watch('src/assets/**', gulp.series('assets'));
});

gulp.task('build', gulp.series(
    'clean',
    gulp.parallel('scss', 'webpack'),
    'assets'
));

gulp.task('dev', gulp.series('build', 'watch'));
